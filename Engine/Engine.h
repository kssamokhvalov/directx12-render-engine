#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include "Render/DeviceResources.h"
#include "Render/ModelManager.h"
#include "Utils/TaskManager/TaskSystem.h"
#include "Render/Renderer.h"
#include "Render/TextureManager.h"
#include "Render/Scene.h"

class Engine
{
public:
	static void Init()
	{
		DX::DeviceResources::GetInstance();
		TaskSystem::GetInstance();
		ModelManager::GetInstance();
	};
	static void Deinit()
	{

	};
};