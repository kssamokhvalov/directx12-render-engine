#include "pch.h"
#include "Window.h"

int Window::createWindow(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams)
{
    WNDCLASSEX wc;

    const LPCWSTR szClassName = L"myWindowClass";

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = WinProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = appHandle;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = szClassName;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wc))
    {
        MessageBox(NULL, L"Window Registration Failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    mHwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        szClassName,
        L"The title of my window",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, mWidthWindow, mHeightWindow,
        NULL, NULL, appHandle, NULL);

    if (mHwnd == NULL)
    {
        MessageBox(NULL, L"Window Creation Failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    if (mWindowedFullscreen)
    {
        SetFullscreen();
    }

    ShowWindow(mHwnd, windowShowParams);

    return 1;
}


void Window::SetFullscreen()
{
    MONITORINFO monitorInfo;
    monitorInfo.cbSize = sizeof(monitorInfo);
    GetMonitorInfo(MonitorFromWindow(mHwnd, MONITOR_DEFAULTTONEAREST),
        &monitorInfo);

    SetWindowPos(mHwnd, NULL, monitorInfo.rcMonitor.left,
        monitorInfo.rcMonitor.top,
        monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
        monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
        SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
}
