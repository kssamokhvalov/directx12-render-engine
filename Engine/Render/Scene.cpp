#include "pch.h"
#include "Scene.h"
#include "Renderer.h"
#include "ModelManager.h"

void Scene::AddModelsToRender()
{
	auto view = mRegistry.view<Renderable, ModelComponent>();

	for (entt::entity entity : view)
	{
		DirectX::XMFLOAT4X4& transform = mRegistry.get<TransformComponent>(entity).transform;
		ModelComponent& modelComponent = mRegistry.get<ModelComponent>(entity);

		Renderer::GetInstance().AddModelToRender(modelComponent, transform);
	}
}

void Scene::AddLightsToRender()
{
	auto view = mRegistry.view<LightComponent, ModelComponent>();

	for (entt::entity entity : view)
	{
		DirectX::XMFLOAT4X4& transform = mRegistry.get<TransformComponent>(entity).transform;
		ModelComponent& modelComponent = mRegistry.get<ModelComponent>(entity);
		LightComponent& lightComponent = mRegistry.get<LightComponent>(entity);

		Renderer::GetInstance().AddLightToRender(modelComponent, lightComponent, transform);
	}
}

void Scene::AddSphereLight(const DirectX::XMFLOAT4X4& transform, DirectX::XMFLOAT3 color)
{
	entt::entity entt = mRegistry.create();
	auto model = ModelManager::GetInstance().GetUnitSphere();
	Renderer::GetInstance().UploadModel(model);

	mRegistry.emplace<TransformComponent>(entt, transform);
	mRegistry.emplace<LightComponent>(entt, color, 1.0f);
	mRegistry.emplace<ModelComponent>(entt, model);

}

void Scene::AddSphereLight(const DirectX::XMFLOAT4X4& transform, LightComponent lightComponent)
{
	entt::entity entt = mRegistry.create();
	auto model = ModelManager::GetInstance().GetUnitSphere();
	Renderer::GetInstance().UploadModel(model);

	DirectX::XMMATRIX tmp = DirectX::XMLoadFloat4x4(&transform);
	tmp.r[0] = DirectX::XMVectorScale(tmp.r[0], lightComponent.influenceDistance);
	tmp.r[1] = DirectX::XMVectorScale(tmp.r[1], lightComponent.influenceDistance);
	tmp.r[2] = DirectX::XMVectorScale(tmp.r[2], lightComponent.influenceDistance);
	DirectX::XMFLOAT4X4 transform_tmp;
	DirectX::XMStoreFloat4x4(&transform_tmp, tmp);

	mRegistry.emplace<TransformComponent>(entt, transform_tmp);
	mRegistry.emplace<LightComponent>(entt, lightComponent);
	mRegistry.emplace<ModelComponent>(entt, model);
}

void Scene::AddModel(ModelComponent modelComponent, const DirectX::XMFLOAT4X4& transform)
{
	entt::entity entt = mRegistry.create();

	Renderer::GetInstance().UploadModel(modelComponent.model);
	for (auto& texture : modelComponent.textures)
	{
		Renderer::GetInstance().UploadTextures(&texture);
	}

	mRegistry.emplace<ModelComponent>(entt, modelComponent);
	mRegistry.emplace<TransformComponent>(entt, transform);
	mRegistry.emplace<Renderable>(entt, Renderable::Opaque);
}
