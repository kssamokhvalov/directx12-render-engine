#pragma once
#include "..\Utils\Singlton.h"
#include "Core/Texture.h"

#include <unordered_map>
#include <memory>
#include <string>

class TextureManager : public Singlton<TextureManager>
{
public:
	Texture* GetTexture(const std::string& path);
private:
	Texture* DDSTextureLoader(const std::string& path);
	std::unordered_map<std::string, std::unique_ptr<Texture>> mLoadedTextures;
};

