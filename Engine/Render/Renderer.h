﻿#pragma once

#include "DeviceResources.h"
#include "Core\GpuBuffer.h"
#include "Core\GpuBufferHeap.h"
#include "Core\DescriptorHeap.h"
#include "Core\RootSignature.h"
#include "Core/RingBuffer.h"
#include "Core/SolidBuffer.h"
#include "Core/Texture.h"
#include "..\Utils\Singlton.h"

#include <array>

class Camera;
class RootSignature;
struct Model;
class Scene;
struct ModelComponent;
struct LightComponent;

// Constant buffer used to send MVP matrices to the vertex shader.
struct ModelViewProjectionConstantBuffer
{
	uint32_t			resolution[2];
	float				rResolution[2];
	DirectX::XMFLOAT4X4 view;
	DirectX::XMFLOAT4X4 projection;
	DirectX::XMFLOAT4X4 projViewInv;
	DirectX::XMFLOAT3	bl;
	DirectX::XMFLOAT3	br;
	DirectX::XMFLOAT3	tl;
	uint32_t			_pad[3];
};

struct ModelConstantBuffer
{
	DirectX::XMFLOAT4X4 model;
	uint32_t			materials;
	uint32_t			_pad[47];
};

struct LightConstantBuffer
{
	DirectX::XMFLOAT4X4 ligth;
	DirectX::XMFLOAT3	I;
	float				radiusLight;
	float				influenceDistance;
	uint32_t			_pad[43];
};

struct GBuffer
{
	Texture GB0;
	Texture GB1;
	Texture GB2;
	Texture Depth;
};

// Used to send per-vertex data to the vertex shader.
struct VertexPositionColor
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 color;
};

// This sample renderer instantiates a basic rendering pipeline.
class Renderer : public Singlton<Renderer>
{
public:
	Renderer();
	~Renderer();
	void Init();
	void CreateDeviceDependentResources();
	void CreateWindowSizeDependentResources();
	void Update();
	void UpdateViewMatrix(const Camera& camera);
	bool DefferedRenderScene(Scene* scene);
	bool LightRenderScene(Scene* scene);
	void AddModelToRender(ModelComponent& modelCoponent, const DirectX::XMFLOAT4X4& transform);
	void AddLightToRender(ModelComponent& modelCoponent, LightComponent& lightComponent, const DirectX::XMFLOAT4X4& transform);

	void UploadBuffer(ID3D12GraphicsCommandList* commandList, GpuBuffer& buffer, GpuBufferHeap& bufferHeap);
	void UploadTexture(ID3D12GraphicsCommandList* commandList, Texture* texture);
	void UploadTextures(ID3D12GraphicsCommandList* commandList, OpaqueTextures* textures);
	void UploadTextures(OpaqueTextures* textures) { UploadTextures(mCommandList.Get(), textures); };

	void UploadModel(ID3D12GraphicsCommandList* commandList, Model* model);
	void UploadModel(Model* model) { UploadModel(mCommandList.Get(), model); }
private:
	// Constant buffers must be 256-byte aligned.
	static const UINT c_alignedConstantBufferSize = sizeof(ModelViewProjectionConstantBuffer);
	static const UINT cAlignedModelConstantBufferSize = sizeof(ModelConstantBuffer);
	static const UINT cNumRTVinGBuffer = 3;

	void PreRenderModelMath(ModelComponent& modelCoponent, const DirectX::XMFLOAT4X4& transform);
	void PreRenderLightMath(ModelComponent& modelCoponent, LightComponent& lightComponent, const DirectX::XMFLOAT4X4& transform);
	void InitGBuffer();
	void InitLightRenderPipline();
	void CopyDepthBuffer();

	// Direct3D resources for cube geometry.
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	mCommandList;
	Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;
	ModelViewProjectionConstantBuffer					mConstantBufferData;
	UINT8* m_mappedConstantBuffer;
	D3D12_RECT											m_scissorRect;
	std::vector<byte>									m_vertexShader;
	std::vector<byte>									m_pixelShader;

	SolidBuffer											mTexturesHeap;
	const uint32_t										NUM_TEXTURS{ 40 };

	static const uint32_t								mNumMatrixs{ 20 };
	std::array<ModelConstantBuffer, mNumMatrixs>		mInstancedMatrix;
	RingBuffer											mModelConstantBuffers;
	DescriptorHeap										mDescriptorHeap;
	DescriptorHeap										mSamplersDescriptorHeap;

	DescriptorHeap										mRTVDescriptorHeap;
	DescriptorHeap										mDSVDescriptorHeap;
	std::array<GBuffer, DX::c_frameCount>				mGBufferResourses;
	Microsoft::WRL::ComPtr<ID3D12PipelineState>			mPSODefferedRender;
	RootSignature										mRootSignatureDefferedRender;

	RootSignature										mRootSignatureLightRender;
	Microsoft::WRL::ComPtr<ID3D12PipelineState>			mPSOLightRender;

	static const uint32_t								mNumLights{ 20 };
	std::array<LightConstantBuffer, mNumMatrixs>		mLightsMatrix;
	RingBuffer											mLightConstantBuffers;
	DescriptorHeap										mLightDescriptorHeap;

	bool	m_loadingComplete;
};

