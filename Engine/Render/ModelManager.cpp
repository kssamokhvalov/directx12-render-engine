#include "pch.h"

#include "ModelManager.h"
#include "Primitives/Model.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <functional>

void ModelManager::Init()
{
}

void ModelManager::Deinit()
{
}

Model* ModelManager::GetUnitSphere()
{
	auto search = mLoadedModels.find(mDefaultModels[DefaultModels::UNIT_SPHERE]);
	if (search != mLoadedModels.end())
		return search->second.get();
	else
		return InitUnitSphere();
}

Model* ModelManager::GetModel(const std::string& path)
{
	auto search = mLoadedModels.find(path);
	if (search != mLoadedModels.end())
		return search->second.get();
	else
		return AssimpLoader(path);
}

Model* ModelManager::AssimpLoader(const std::string& path)
{
	// Load aiScene

	uint32_t flags = uint32_t(aiProcess_Triangulate | aiProcess_GenBoundingBoxes | aiProcess_ConvertToLeftHanded | aiProcess_CalcTangentSpace);
	// aiProcess_Triangulate - ensure that all faces are triangles and not polygonals, otherwise triangulare them
	// aiProcess_GenBoundingBoxes - automatically compute bounding box, though we could do that manually
	// aiProcess_ConvertToLeftHanded - Assimp assumes left-handed basis orientation by default, convert for Direct3D
	// aiProcess_CalcTangentSpace - computes tangents and bitangents, they are used in advanced lighting

	Assimp::Importer importer;
	const aiScene* assimpScene = importer.ReadFile(path, flags);
	//DEV_ASSERT(assimpScene);

	uint32_t numMeshes = assimpScene->mNumMeshes;
	uint32_t numMaterials = assimpScene->mNumMaterials;

	// Load vertex data

	std::shared_ptr<Model> model = std::make_shared<Model>();
	model->mName = path;
	model->mBoundingBox = {};
	model->mMeshes.resize(numMeshes);
	//model->albedo_texture_paths.resize(numMaterials);

	static_assert(sizeof(DirectX::XMFLOAT3) == sizeof(aiVector3D), "DirectX::XMFLOAT3 is not equal to aiVector3D");
	static_assert(sizeof(Face) == 3 * sizeof(uint32_t), "math::Triangle is not equal to 3 * uint32_t");

	// Recursively load mesh instances (meshToModel transformation matrices)

	std::function<void(aiNode*)> loadInstances;
	loadInstances = [&loadInstances, &model](aiNode* node)
	{
		DirectX::XMFLOAT4X4 nodeToParent = reinterpret_cast<const DirectX::XMFLOAT4X4&>(node->mTransformation.Transpose());
		DirectX::XMFLOAT4X4 parentToNode;
		DirectX::XMMATRIX vNodeToParent = DirectX::XMLoadFloat4x4(&nodeToParent);
		DirectX::XMMATRIX vParentToNode = DirectX::XMMatrixInverse(nullptr, vNodeToParent);
		DirectX::XMStoreFloat4x4(&nodeToParent, vNodeToParent);
		DirectX::XMStoreFloat4x4(&parentToNode, vParentToNode);

		// The same node may contain multiple meshes in its space, referring to them by indices
		for (uint32_t i = 0; i < node->mNumMeshes; ++i)
		{
			uint32_t meshIndex = node->mMeshes[i];
			model->mMeshes[meshIndex].mInstances.push_back(nodeToParent);
			model->mMeshes[meshIndex].mInstancesInv.push_back(parentToNode);
		}

		for (uint32_t i = 0; i < node->mNumChildren; ++i)
			loadInstances(node->mChildren[i]);
	};

	loadInstances(assimpScene->mRootNode);

	//Load vertex and calculate bounding box

	DirectX::XMFLOAT3 minModel = reinterpret_cast<DirectX::XMFLOAT3&>(assimpScene->mMeshes[0]->mAABB.mMin);
	DirectX::XMFLOAT3 maxModel = reinterpret_cast<DirectX::XMFLOAT3&>(assimpScene->mMeshes[0]->mAABB.mMax);

	for (uint32_t i = 0; i < numMeshes; ++i)
	{
		auto& srcMesh = assimpScene->mMeshes[i];
		auto& dstMesh = model->mMeshes[i];

		dstMesh.mName = srcMesh->mName.C_Str();
		DirectX::XMFLOAT3 min = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mAABB.mMin);
		DirectX::XMFLOAT3 max = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mAABB.mMax);
		DirectX::XMVECTOR vMin = DirectX::XMLoadFloat3(&min);
		DirectX::XMVECTOR vMax = DirectX::XMLoadFloat3(&max);

		DirectX::XMStoreFloat3(&dstMesh.mBoundingBox.Center, DirectX::XMVectorScale(DirectX::XMVectorAdd(vMin, vMax), 0.5f));
		DirectX::XMStoreFloat3(&dstMesh.mBoundingBox.Extents, DirectX::XMVectorScale(DirectX::XMVectorSubtract(vMax, vMin), 0.5f));

		DirectX::XMMATRIX vNodeToParent = DirectX::XMLoadFloat4x4(&dstMesh.mInstances[0]);				//TODO Must be more than 1 element in vector!!!!!

		vMin = DirectX::XMVector4Transform(vMin, vNodeToParent);
		vMax = DirectX::XMVector4Transform(vMax, vNodeToParent);
		DirectX::XMStoreFloat3(&min, vMin);
		DirectX::XMStoreFloat3(&max, vMax);

		minModel.x = std::fminf(minModel.x, min.x);
		minModel.y = std::fminf(minModel.y, min.y);
		minModel.z = std::fminf(minModel.z, min.z);

		maxModel.x = std::fmaxf(maxModel.x, max.x);
		maxModel.y = std::fmaxf(maxModel.y, max.y);
		maxModel.z = std::fmaxf(maxModel.z, max.z);


		dstMesh.mVertices.resize(srcMesh->mNumVertices);
		dstMesh.mFaces.resize(srcMesh->mNumFaces);

		for (uint32_t v = 0; v < srcMesh->mNumVertices; ++v)
		{
			Mesh::Vertex& vertex = dstMesh.mVertices[v];
			vertex.position = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mVertices[v]);
			vertex.tc = reinterpret_cast<DirectX::XMFLOAT2&>(srcMesh->mTextureCoords[0][v]);
			vertex.normal = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mNormals[v]);
			vertex.tangent = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mTangents[v]);

			DirectX::XMFLOAT3 bitangent = reinterpret_cast<DirectX::XMFLOAT3&>(srcMesh->mBitangents[v]);
			DirectX::XMVECTOR vBitangent = DirectX::XMLoadFloat3(&bitangent);
			DirectX::XMStoreFloat3(&vertex.bitangent, DirectX::XMVectorScale(vBitangent, -1.0f));	// Flip V
		}

		for (uint32_t f = 0; f < srcMesh->mNumFaces; ++f)
		{
			const auto& face = srcMesh->mFaces[f];
			//DEV_ASSERT(face.mNumIndices == 3);
			dstMesh.mFaces[f] = *reinterpret_cast<Face*>(face.mIndices);
		}

		dstMesh.Init();
	}

	DirectX::XMVECTOR vMin = DirectX::XMLoadFloat3(&minModel);
	DirectX::XMVECTOR vMax = DirectX::XMLoadFloat3(&maxModel);

	DirectX::XMStoreFloat3(&model->mBoundingBox.Center, DirectX::XMVectorScale(DirectX::XMVectorAdd(vMin, vMax), 0.5f));
	DirectX::XMStoreFloat3(&model->mBoundingBox.Extents, DirectX::XMVectorScale(DirectX::XMVectorSubtract(vMax, vMin), 0.5f));

	mLoadedModels.emplace(path, model);
	return model.get();
}

Model* ModelManager::InitUnitSphere()
{
	const uint32_t SIDES = 6;
	const uint32_t GRID_SIZE = 12;
	const uint32_t TRIS_PER_SIDE = GRID_SIZE * GRID_SIZE * 2;
	const uint32_t VERT_PER_SIZE = (GRID_SIZE + 1) * (GRID_SIZE + 1);

	std::shared_ptr <Model> model(new Model);
	model->mName = "UNIT_SPHERE";
	model->mBoundingBox = DirectX::BoundingBox();

	model->mMeshes.push_back(Mesh());
	Mesh& mesh = model->mMeshes[model->mMeshes.size() - 1];
	mesh.mName = "UNIT_SPHERE";
	mesh.mBoundingBox = model->mBoundingBox;
	mesh.mInstances.resize(1);
	mesh.mInstancesInv.resize(1);
	DirectX::XMStoreFloat4x4(&mesh.mInstances[0], DirectX::XMMatrixIdentity());
	DirectX::XMStoreFloat4x4(&mesh.mInstancesInv[0], DirectX::XMMatrixIdentity());

	mesh.mVertices.resize(VERT_PER_SIZE * SIDES);
	Mesh::Vertex* vertex = mesh.mVertices.data();

	int sideMasks[6][3] =
	{
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 2, 1, 0 },
		{ 0, 1, 2 },
		{ 0, 2, 1 },
		{ 0, 2, 1 }
	};

	float sideSigns[6][3] =
	{
		{ +1, +1, +1 },
		{ -1, +1, +1 },
		{ -1, +1, -1 },
		{ +1, +1, -1 },
		{ +1, -1, -1 },
		{ +1, +1, +1 }
	};

	auto tmpFunc = [](DirectX::XMFLOAT3& vertex, int index, float val)
	{
		switch (index)
		{
		case 0:
			vertex.x = val;
			break;
		case 1:
			vertex.y = val;
			break;
		default:
			vertex.z = val;
		}
	};

	for (int side = 0; side < SIDES; ++side)
	{
		for (int row = 0; row < GRID_SIZE + 1; ++row)
		{
			for (int col = 0; col < GRID_SIZE + 1; ++col)
			{
				DirectX::XMFLOAT3 v;
				v.x = col / float(GRID_SIZE) * 2.f - 1.f;
				v.y = row / float(GRID_SIZE) * 2.f - 1.f;
				v.z = 1.f;

				//vertex[0] = engine::Mesh::Vertex::initial();

				tmpFunc(vertex[0].position, sideMasks[side][0], v.x * sideSigns[side][0]);
				tmpFunc(vertex[0].position, sideMasks[side][1], v.y * sideSigns[side][1]);
				tmpFunc(vertex[0].position, sideMasks[side][2], v.z * sideSigns[side][2]);
				float rLenght = 1.0f / sqrtf(vertex[0].position.x * vertex[0].position.x + vertex[0].position.y * vertex[0].position.y + vertex[0].position.z * vertex[0].position.z);
				vertex[0].position.x = vertex[0].position.x * rLenght;
				vertex[0].position.y = vertex[0].position.y * rLenght;
				vertex[0].position.z = vertex[0].position.z * rLenght;

				vertex[0].normal = vertex[0].position;

				vertex += 1;
			}
		}
	}

	mesh.mFaces.resize(TRIS_PER_SIDE * SIDES);
	auto* triangle = mesh.mFaces.data();

	for (int side = 0; side < SIDES; ++side)
	{
		uint32_t sideOffset = VERT_PER_SIZE * side;

		for (int row = 0; row < GRID_SIZE; ++row)
		{
			for (int col = 0; col < GRID_SIZE; ++col)
			{
				triangle[0].indexs[0] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 0;
				triangle[0].indexs[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
				triangle[0].indexs[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

				triangle[1].indexs[0] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 0;
				triangle[1].indexs[1] = sideOffset + (row + 1) * (GRID_SIZE + 1) + col + 1;
				triangle[1].indexs[2] = sideOffset + (row + 0) * (GRID_SIZE + 1) + col + 1;

				triangle += 2;
			}
		}
	}
	mesh.Init();
	mLoadedModels.emplace(mDefaultModels[UNIT_SPHERE], model);

	return model.get();
}
