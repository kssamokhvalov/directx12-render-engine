#pragma once
#include <DirectXMath.h>
class Camera
{
public:
	Camera() : mFixedBottom(true){}

	void SetPerspective			(float fov, float aspect, float near_plane, float far_plane);
	void SetWorldPosition		(const DirectX::XMVECTOR& position) { mPosition = position; }
	void AddWorldPosition		(const DirectX::XMVECTOR& offset)	{ mPosition = DirectX::XMVectorAdd(mPosition, offset); }
	void AddRelativePosition	(const DirectX::XMVECTOR& offset);
	
	void SetWorldAngles			(const DirectX::XMVECTOR& rotation) { mRotation = rotation; }
	void AddWorldAngles			(const DirectX::XMVECTOR& rotation);
	void AddRelativeAngles		(const DirectX::XMVECTOR& rotation);

	const DirectX::XMVECTOR& GetRotation() const { return mRotation; }
	const DirectX::XMVECTOR& GetPosition() const { return mPosition; }
	const DirectX::XMMATRIX& GetViewMatrix() const { return mView; }
	const DirectX::XMMATRIX& GetViewInvMatrix() const { return mViewInv; }

	const DirectX::XMVECTOR Right() 	    const { return mViewInv.r[0]; }
	const DirectX::XMVECTOR Top() 			const { return mViewInv.r[1]; }
	const DirectX::XMVECTOR Forward() 		const { return mViewInv.r[2]; }
	const DirectX::XMVECTOR Position()		const { return mViewInv.r[3]; }

	void UpdateMatrix();
private:
	DirectX::XMVECTOR		mRotation;

	DirectX::XMVECTOR 		mPosition;

	DirectX::XMMATRIX 		mProjction;
	DirectX::XMMATRIX 		mProjctionInv;
	DirectX::XMMATRIX 		mView;
	DirectX::XMMATRIX 		mViewInv;
	DirectX::XMMATRIX 		mViewProjction;
	DirectX::XMMATRIX 		mViewProjctionInv;

	bool					mFixedBottom;
};

