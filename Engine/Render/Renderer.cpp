﻿#include "pch.h"

#include "Renderer.h"
#include "DirectXHelper.h"
#include "Camera.h"
#include "Primitives/Model.h"
#include "Core/RootSignature.h"
#include "Core/DescriptorHeap.h"
#include "Core/GpuBuffer.h"
#include "Core/GpuBufferHeap.h"
#include "TextureManager.h"
#include "Scene.h"

#include <D3DCompiler.h>
#include <iostream>
#include <fstream>
#include <direct.h>
#include <stddef.h>

#pragma comment(lib,"d3dcompiler.lib")

using namespace DirectX;
using namespace Microsoft::WRL;


// Loads vertex and pixel shaders from files and instantiates the cube geometry.
Renderer::Renderer() :
	m_loadingComplete(false),
	m_mappedConstantBuffer(nullptr)
{

}

Renderer::~Renderer()
{
	m_constantBuffer->Unmap(0, nullptr);

	m_mappedConstantBuffer = nullptr;

	mModelConstantBuffers.Deinit();
}

void Renderer::Init()
{
	ZeroMemory(&mConstantBufferData, sizeof(mConstantBufferData));

	CreateDeviceDependentResources();
	CreateWindowSizeDependentResources();
}

void Renderer::CreateDeviceDependentResources()
{
	auto d3dDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	InitGBuffer();
	InitLightRenderPipline();

	// Create and upload cube geometry resources to the GPU.
	{

		// Create a command list.
		DX::ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, DX::DeviceResources::GetInstance().GetCommandAllocator(), mPSODefferedRender.Get(), IID_PPV_ARGS(&mCommandList)));
        NAME_D3D12_OBJECT(mCommandList);

		CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);

		mModelConstantBuffers.Init(cAlignedModelConstantBufferSize);
		mLightConstantBuffers.Init(sizeof(LightConstantBuffer));

		CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(DX::c_frameCount * c_alignedConstantBufferSize);
		DX::ThrowIfFailed(d3dDevice->CreateCommittedResource(
			&uploadHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&m_constantBuffer)));

        NAME_D3D12_OBJECT(m_constantBuffer);

		mDescriptorHeap.Create(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, DX::c_frameCount + mModelConstantBuffers.GetLenghtBuffer() + (NUM_TEXTURS * 4));
		
		mTexturesHeap.Init(&mDescriptorHeap, DX::c_frameCount + mModelConstantBuffers.GetLenghtBuffer(), NUM_TEXTURS);

		{
			// Create constant buffer views to access the upload buffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = m_constantBuffer->GetGPUVirtualAddress();
			mDescriptorHeap.SetConstantBufferViews(0, DX::c_frameCount, cbvGpuAddress, c_alignedConstantBufferSize);
		}

		{
			// Create constant buffer views to access the upload buffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = mModelConstantBuffers.GetGPUVirtualAddress();
			mDescriptorHeap.SetConstantBufferViews(DX::c_frameCount, mModelConstantBuffers.GetLenghtBuffer(), cbvGpuAddress, mModelConstantBuffers.GetBufferSize());
			mModelConstantBuffers.SetDescriptorHeapDesc(mDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), mDescriptorHeap.GetDescriptorSize(), DX::c_frameCount);
		}

		mSamplersDescriptorHeap.Create(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER, 1);
		{
			D3D12_SAMPLER_DESC desc{};
			desc.Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
			desc.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			desc.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			desc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			desc.MinLOD = 0;
			desc.MaxLOD = D3D12_FLOAT32_MAX;
			desc.MipLODBias = 0.0f;
			desc.MaxAnisotropy = 1;
			desc.ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
			mSamplersDescriptorHeap.SetSampler(desc);
		}

		mLightDescriptorHeap.Create(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + (DX::c_frameCount * 4));

		{
			// Create constant buffer views to access the upload buffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = m_constantBuffer->GetGPUVirtualAddress();
			mLightDescriptorHeap.SetConstantBufferViews(0, DX::c_frameCount, cbvGpuAddress, c_alignedConstantBufferSize);
		}

		{
			// Create constant buffer views to access the upload buffer.
			D3D12_GPU_VIRTUAL_ADDRESS cbvGpuAddress = mLightConstantBuffers.GetGPUVirtualAddress();
			mLightDescriptorHeap.SetConstantBufferViews(DX::c_frameCount, mLightConstantBuffers.GetLenghtBuffer(), cbvGpuAddress, mLightConstantBuffers.GetBufferSize());
			mLightConstantBuffers.SetDescriptorHeapDesc(mLightDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), mLightDescriptorHeap.GetDescriptorSize(), DX::c_frameCount);
		}

		{
			for (int i = 0; i < DX::c_frameCount; ++i)
			{
				mLightDescriptorHeap.SetTextureAsShaderResourseView(mGBufferResourses[i].GB0.GetResource(), DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + i * 4 + 0);
				mLightDescriptorHeap.SetTextureAsShaderResourseView(mGBufferResourses[i].GB1.GetResource(), DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + i * 4 + 1);
				mLightDescriptorHeap.SetTextureAsShaderResourseView(mGBufferResourses[i].GB2.GetResource(), DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + i * 4 + 2);
				mLightDescriptorHeap.SetTextureAsShaderResourseView(mGBufferResourses[i].Depth.GetResource(), DXGI_FORMAT_R32_FLOAT, DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + i * 4 + 3);
			}
		}

		// Map the constant buffers.
		CD3DX12_RANGE readRange(0, 0);		// We do not intend to read from this resource on the CPU.
		DX::ThrowIfFailed(m_constantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&m_mappedConstantBuffer)));

		ZeroMemory(m_mappedConstantBuffer, DX::c_frameCount * c_alignedConstantBufferSize);


		// We don't unmap this until the app closes. Keeping things mapped for the lifetime of the resource is okay.

		// Close the command list and execute it to begin the vertex/index buffer copy into the GPU's default heap.
		DX::DeviceResources::GetInstance().ExecuteCommandList(mCommandList.Get());
	}

	
	m_loadingComplete = true;
}

// Initializes view parameters when the window size changes.
void Renderer::CreateWindowSizeDependentResources()
{
	uint32_t width = DX::DeviceResources::GetInstance().GetWidth();
	uint32_t height = DX::DeviceResources::GetInstance().GetHeight();
	float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
	float fovAngleY = 70.0f * XM_PI / 180.0f;

	D3D12_VIEWPORT viewport = DX::DeviceResources::GetInstance().GetScreenViewport();
	m_scissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height)};

	// This is a simple example of change that can be made when the app is in
	// portrait or snapped view.
	if (aspectRatio < 1.0f)
	{
		fovAngleY *= 2.0f;
	}

	// Note that the OrientationTransform3D matrix is post-multiplied here
	// in order to correctly orient the scene to match the display orientation.
	// This post-multiplication step is required for any draw calls that are
	// made to the swap chain render target. For draw calls to other targets,
	// this transform should not be applied.

	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovLH(
		fovAngleY,
		aspectRatio,
		100.0f,
		0.01f
		);

	XMStoreFloat4x4(
		&mConstantBufferData.projection,
		XMMatrixTranspose(perspectiveMatrix)
		);

	// Eye is at (0,0.7,1.5), looking at point (0,-0.1,0) with the up-vector along the y-axis.
	static const XMVECTORF32 eye = { 0.0f, 0.7f, 1.5f, 0.0f };
	static const XMVECTORF32 at = { 0.0f, -0.1f, 0.0f, 0.0f };
	static const XMVECTORF32 up = { 0.0f, 1.0f, 0.0f, 0.0f };

	XMStoreFloat4x4(&mConstantBufferData.view, XMMatrixTranspose(XMMatrixLookAtRH(eye, at, up)));
	mConstantBufferData.resolution[0] = DX::DeviceResources::GetInstance().GetWidth();
	mConstantBufferData.resolution[1] = DX::DeviceResources::GetInstance().GetHeight();
	mConstantBufferData.rResolution[0] = 1.0f / static_cast<float>(mConstantBufferData.resolution[0]);
	mConstantBufferData.rResolution[1] = 1.0f / static_cast<float>(mConstantBufferData.resolution[1]);
}

// Called once per frame, rotates the cube and calculates the model and view matrices.
void Renderer::Update()
{
	if (m_loadingComplete)
	{
		// Update the constant buffer resource.
		UINT8* destination = m_mappedConstantBuffer + (DX::DeviceResources::GetInstance().GetCurrentFrameIndex() * c_alignedConstantBufferSize);
		memcpy(destination, &mConstantBufferData, sizeof(mConstantBufferData));
	}
}

void Renderer::UpdateViewMatrix(const Camera& camera)
{
	XMStoreFloat4x4(&mConstantBufferData.view, XMMatrixTranspose(camera.GetViewMatrix()));

	XMMATRIX viewInv = XMMatrixTranspose(camera.GetViewInvMatrix());
	XMMATRIX proj = XMLoadFloat4x4(&mConstantBufferData.projection);
	XMMATRIX projInv = XMMatrixInverse(nullptr, proj);

	XMStoreFloat4x4(&mConstantBufferData.projViewInv, XMMatrixMultiply(viewInv, projInv));

	XMVECTOR BL = XMVector4Transform(XMVectorSet(-1.0f, -1.0f, 1.0f, 1.0f), projInv);
	XMVECTOR BR = XMVector4Transform(XMVectorSet(1.0f, -1.0f, 1.0f, 1.0f), projInv);
	XMVECTOR TL = XMVector4Transform(XMVectorSet(-1.0f, 1.0f, 1.0f, 1.0f), projInv);

	XMVectorScale(BL, XMVectorGetW(BL));
	XMVectorScale(BR, XMVectorGetW(BR));
	XMVectorScale(TL, XMVectorGetW(TL));


	BR = XMVectorSubtract(BR, BL);
	TL = XMVectorSubtract(TL, BL);

	XMVectorSetW(BL, .0f);
	XMVectorSetW(BR, .0f);
	XMVectorSetW(TL, .0f);

	BL = XMVector4Transform(BL, viewInv);
	BR = XMVector4Transform(BR, viewInv);
	TL = XMVector4Transform(TL, viewInv);

	XMStoreFloat3(&mConstantBufferData.bl, BL);
	XMStoreFloat3(&mConstantBufferData.br, BR);
	XMStoreFloat3(&mConstantBufferData.tl, TL);
}

bool Renderer::DefferedRenderScene(Scene* scene)
{
	if (!m_loadingComplete)
	{
		return false;
	}

	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetCommandAllocator()->Reset());

	// The command list can be reset anytime after ExecuteCommandList() is called.
	DX::ThrowIfFailed(mCommandList->Reset(DX::DeviceResources::GetInstance().GetCommandAllocator(), mPSODefferedRender.Get()));

	{
		// Set the graphics root signature and descriptor heaps to be used by this frame.
		mCommandList->SetGraphicsRootSignature(mRootSignatureDefferedRender.Get());
		ID3D12DescriptorHeap* ppHeaps[] = { mDescriptorHeap.GetDescriptorHeap(), mSamplersDescriptorHeap.GetDescriptorHeap() };
		mCommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);


		UINT descriptorSize = mDescriptorHeap.GetDescriptorSize();
		// Bind the current frame's constant buffer to the pipeline.
		CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(mDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), DX::DeviceResources::GetInstance().GetCurrentFrameIndex(), descriptorSize);
		mCommandList->SetGraphicsRootDescriptorTable(0, gpuHandle);

		gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(mSamplersDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), 0, mSamplersDescriptorHeap.GetDescriptorSize());

		mCommandList->SetGraphicsRootDescriptorTable(3, gpuHandle); //Sampler

		// Set the viewport and scissor rectangle.
		D3D12_VIEWPORT viewport = DX::DeviceResources::GetInstance().GetScreenViewport();
		mCommandList->RSSetViewports(1, &viewport);
		mCommandList->RSSetScissorRects(1, &m_scissorRect);

		UINT currentFrame = DX::DeviceResources::GetInstance().GetCurrentFrameIndex();

		// Record drawing commands.
		D3D12_CPU_DESCRIPTOR_HANDLE renderTargetViews[cNumRTVinGBuffer];
		for (int i = 0; i < cNumRTVinGBuffer; i++)
		{
			renderTargetViews[i] = CD3DX12_CPU_DESCRIPTOR_HANDLE(mRTVDescriptorHeap.GetCPUDescriptorHandleForHeapStart(), cNumRTVinGBuffer * currentFrame + i, mRTVDescriptorHeap.GetDescriptorSize());
			mCommandList->ClearRenderTargetView(renderTargetViews[i], DirectX::Colors::Black, 0, nullptr);
		}


		CD3DX12_CPU_DESCRIPTOR_HANDLE depthStencilView = DX::DeviceResources::GetInstance().GetDepthStencilView();
		mCommandList->ClearDepthStencilView(depthStencilView, D3D12_CLEAR_FLAG_DEPTH, 0.0f, 0, 0, nullptr);

		mCommandList->OMSetRenderTargets(cNumRTVinGBuffer, renderTargetViews, false, &depthStencilView);

		mCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		scene->AddModelsToRender();
	}

	DX::DeviceResources::GetInstance().ExecuteCommandList(mCommandList.Get());
	return true;
}

bool Renderer::LightRenderScene(Scene* scene)
{
	if (!m_loadingComplete)
	{
		return false;
	}

	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetCommandAllocator()->Reset());

	// The command list can be reset anytime after ExecuteCommandList() is called.
	DX::ThrowIfFailed(mCommandList->Reset(DX::DeviceResources::GetInstance().GetCommandAllocator(), mPSOLightRender.Get()));

	{
		// Set the graphics root signature and descriptor heaps to be used by this frame.
		mCommandList->SetGraphicsRootSignature(mRootSignatureLightRender.Get());
		ID3D12DescriptorHeap* ppHeaps[] = { mLightDescriptorHeap.GetDescriptorHeap(), mSamplersDescriptorHeap.GetDescriptorHeap() };
		mCommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);


		UINT descriptorSize = mLightDescriptorHeap.GetDescriptorSize();
		// Bind the current frame's constant buffer to the pipeline.
		CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(mLightDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), DX::DeviceResources::GetInstance().GetCurrentFrameIndex(), descriptorSize);
		mCommandList->SetGraphicsRootDescriptorTable(0, gpuHandle);

		gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(mLightDescriptorHeap.GetGPUDescriptorHandleForHeapStart(), DX::c_frameCount + mLightConstantBuffers.GetLenghtBuffer() + DX::DeviceResources::GetInstance().GetCurrentFrameIndex() * 4, descriptorSize);
		mCommandList->SetGraphicsRootDescriptorTable(2, gpuHandle);

		// Set the viewport and scissor rectangle.
		D3D12_VIEWPORT viewport = DX::DeviceResources::GetInstance().GetScreenViewport();
		mCommandList->RSSetViewports(1, &viewport);
		mCommandList->RSSetScissorRects(1, &m_scissorRect);


		// Record drawing commands.
		CopyDepthBuffer();

		CD3DX12_RESOURCE_BARRIER renderTargetResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(DX::DeviceResources::GetInstance().GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
		mCommandList->ResourceBarrier(1, &renderTargetResourceBarrier);


		D3D12_CPU_DESCRIPTOR_HANDLE renderTargetView = DX::DeviceResources::GetInstance().GetRenderTargetView();
		mCommandList->ClearRenderTargetView(renderTargetView, DirectX::Colors::Black, 0, nullptr);


		CD3DX12_CPU_DESCRIPTOR_HANDLE depthStencilView = DX::DeviceResources::GetInstance().GetDepthStencilView();

		mCommandList->OMSetRenderTargets(1, &renderTargetView, false, &depthStencilView);

		mCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		scene->AddLightsToRender();

		CD3DX12_RESOURCE_BARRIER presentResourceBarrier =
			CD3DX12_RESOURCE_BARRIER::Transition(DX::DeviceResources::GetInstance().GetRenderTarget(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
		mCommandList->ResourceBarrier(1, &presentResourceBarrier);
	}

	DX::DeviceResources::GetInstance().ExecuteCommandList(mCommandList.Get());
	return true;
}

void Renderer::AddModelToRender(ModelComponent& modelCoponent, const DirectX::XMFLOAT4X4& transform)
{
	auto& model = modelCoponent.model;

	PreRenderModelMath(modelCoponent, transform);
	uint32_t descriptorSize = mDescriptorHeap.GetDescriptorSize();

	for (int i = 0; i < model->mMeshes.size(); i++)
	{
		CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle = mModelConstantBuffers.Pop();
		mCommandList->SetGraphicsRootDescriptorTable(1, gpuHandle);
		gpuHandle = mTexturesHeap[modelCoponent.textures[i].idInDescriptorHeap];
		mCommandList->SetGraphicsRootDescriptorTable(2, gpuHandle);

		mCommandList->IASetVertexBuffers(0, 1, &(model->mMeshes[i].mVertexBufferView));
		mCommandList->IASetIndexBuffer(&(model->mMeshes[i].mIndexBufferView));
		mCommandList->DrawIndexedInstanced(model->mMeshes[i].mFaces.size() * 3, 1, 0, 0, 0);
	}
}

void Renderer::AddLightToRender(ModelComponent& modelCoponent, LightComponent& lightComponent, const DirectX::XMFLOAT4X4& transform)
{
	auto& model = modelCoponent.model;

	PreRenderLightMath(modelCoponent, lightComponent, transform);
	uint32_t descriptorSize = mDescriptorHeap.GetDescriptorSize();

	for (int i = 0; i < model->mMeshes.size(); i++)
	{
		CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle = mLightConstantBuffers.Pop();
		mCommandList->SetGraphicsRootDescriptorTable(1, gpuHandle);
		mCommandList->IASetVertexBuffers(0, 1, &(model->mMeshes[i].mVertexBufferView));
		mCommandList->IASetIndexBuffer(&(model->mMeshes[i].mIndexBufferView));
		mCommandList->DrawIndexedInstanced(model->mMeshes[i].mFaces.size() * 3, 1, 0, 0, 0);
	}
}


void Renderer::UploadBuffer(ID3D12GraphicsCommandList* commandList, GpuBuffer& buffer, GpuBufferHeap& bufferHeap) //TODO maybe error with GpuBuffer& buffer becouse mainoly I load IndexBuffer or VertexBuffer
{
	D3D12_SUBRESOURCE_DATA desc = {};
	desc.pData = reinterpret_cast<BYTE*>(bufferHeap.GetData());
	desc.RowPitch = buffer.GetBufferSize();
	desc.SlicePitch = desc.RowPitch;

	UpdateSubresources(commandList, buffer.GetResource(), bufferHeap.GetResource(), 0, 0, 1, &desc);
	CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(buffer.GetResource(), D3D12_RESOURCE_STATE_COPY_DEST, buffer.GetUsageState());
	mCommandList->ResourceBarrier(1, &vertexBufferResourceBarrier);
}

void Renderer::UploadTexture(ID3D12GraphicsCommandList* commandList, Texture* texture)
{
	if (texture->GetCurrentState() != D3D12_RESOURCE_STATE_COMMON)
		return;

	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetCommandAllocator()->Reset());

	DX::ThrowIfFailed(commandList->Reset(DX::DeviceResources::GetInstance().GetCommandAllocator(), mPSODefferedRender.Get()));

	CD3DX12_RESOURCE_BARRIER textureResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(texture->GetResource(),
			D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_COPY_DEST);
	commandList->ResourceBarrier(1, &textureResourceBarrier);

	UINT numSubresourses = texture->GetSubresources().size();
	D3D12_SUBRESOURCE_DATA* subresourses = texture->GetSubresources().data();
	UpdateSubresources(commandList, texture->GetResource(), texture->GetTextureHeap()->GetResource(), 0, 0, numSubresourses, subresourses);

	textureResourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(texture->GetResource(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	commandList->ResourceBarrier(1, &textureResourceBarrier);

	DX::DeviceResources::GetInstance().ExecuteCommandList(commandList);

	texture->SetState(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
}

void Renderer::UploadTextures(ID3D12GraphicsCommandList* commandList, OpaqueTextures* textures)
{
	mTexturesHeap.Add(commandList, mPSODefferedRender.Get(), *textures);

	UploadTexture(commandList, textures->albedo);
	UploadTexture(commandList, textures->normal);
	UploadTexture(commandList, textures->roughness);
	UploadTexture(commandList, textures->metalness);
}

void Renderer::UploadModel(ID3D12GraphicsCommandList* commandList, Model* model)
{
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetCommandAllocator()->Reset());

	// The command list can be reset anytime after ExecuteCommandList() is called.
	DX::ThrowIfFailed(mCommandList->Reset(DX::DeviceResources::GetInstance().GetCommandAllocator(), mPSODefferedRender.Get()));
	for(auto& mesh : model->mMeshes)
	{
		UploadBuffer(mCommandList.Get(), mesh.mVertexBuffer, mesh.mVertexBufferUpload);
		UploadBuffer(mCommandList.Get(), mesh.mIndexBuffer, mesh.mIndexBufferUpload);
	}

	DX::DeviceResources::GetInstance().ExecuteCommandList(mCommandList.Get());
}

void Renderer::PreRenderModelMath(ModelComponent& modelCoponent, const DirectX::XMFLOAT4X4& transform)
{
	auto& meshes = modelCoponent.model->mMeshes;

	FXMMATRIX modelToWorld = DirectX::XMLoadFloat4x4(&transform);
	for (size_t i = 0; i < meshes.size(); i++)
	{
		FXMMATRIX meshToModel = DirectX::XMLoadFloat4x4(&(meshes[i].mInstances[0]));
		FXMMATRIX meshToWorld = DirectX::XMMatrixMultiply(meshToModel, modelToWorld);
		DirectX::XMMatrixTranspose(meshToWorld);
		DirectX::XMStoreFloat4x4(&mInstancedMatrix[i].model, DirectX::XMMatrixTranspose(meshToWorld));
		mInstancedMatrix[i].materials = modelCoponent.textures[i].hasTextures;
	}

	mModelConstantBuffers.Push(mInstancedMatrix.data(), meshes.size() * cAlignedModelConstantBufferSize);
}

void Renderer::PreRenderLightMath(ModelComponent& modelCoponent, LightComponent& lightComponent, const DirectX::XMFLOAT4X4& transform)
{
	auto& meshes = modelCoponent.model->mMeshes;

	FXMMATRIX modelToWorld = DirectX::XMLoadFloat4x4(&transform);
	for (size_t i = 0; i < meshes.size(); i++)
	{
		FXMMATRIX meshToModel = DirectX::XMLoadFloat4x4(&(meshes[i].mInstances[0]));
		FXMMATRIX meshToWorld = DirectX::XMMatrixMultiply(meshToModel, modelToWorld);
		DirectX::XMMatrixTranspose(meshToWorld);
		DirectX::XMStoreFloat4x4(&mLightsMatrix[i].ligth, DirectX::XMMatrixTranspose(meshToWorld));

		XMVECTOR I = XMLoadFloat3(&lightComponent.color);
		I = XMVectorScale(I, lightComponent.intensity);
		XMStoreFloat3(&mLightsMatrix[i].I, I);
		mLightsMatrix[i].radiusLight = lightComponent.radiusLight;
		mLightsMatrix[i].influenceDistance = lightComponent.influenceDistance;
	}

	mLightConstantBuffers.Push(mLightsMatrix.data(), meshes.size() * sizeof(LightConstantBuffer));
}

void Renderer::InitGBuffer()
{
	{
		D3D12_RESOURCE_DESC gbufferDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT, DX::DeviceResources::GetInstance().GetWidth(), DX::DeviceResources::GetInstance().GetHeight(), 1, 1);
		gbufferDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
		
		D3D12_RESOURCE_DESC depthDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS, DX::DeviceResources::GetInstance().GetWidth(), DX::DeviceResources::GetInstance().GetHeight(), 1, 1);

		for (auto& gbuffer : mGBufferResourses)
		{
			gbuffer.GB0.CreateTextureForRT(gbufferDesc);
			gbuffer.GB1.CreateTextureForRT(gbufferDesc);
			gbuffer.GB2.CreateTextureForRT(gbufferDesc);
			gbuffer.Depth.CreateTexture(depthDesc);
		}

		mRTVDescriptorHeap.Create(D3D12_DESCRIPTOR_HEAP_TYPE_RTV, cNumRTVinGBuffer * DX::c_frameCount, D3D12_DESCRIPTOR_HEAP_FLAG_NONE);
		D3D12_RENDER_TARGET_VIEW_DESC rtvDesc{};

		for (int i = 0; i < DX::c_frameCount; i++)
		{
			mRTVDescriptorHeap.SetRenderTargetView(mGBufferResourses[i].GB0.GetResource(), rtvDesc, i * cNumRTVinGBuffer + 0);
			mRTVDescriptorHeap.SetRenderTargetView(mGBufferResourses[i].GB1.GetResource(), rtvDesc, i * cNumRTVinGBuffer + 1);
			mRTVDescriptorHeap.SetRenderTargetView(mGBufferResourses[i].GB2.GetResource(), rtvDesc, i * cNumRTVinGBuffer + 2);
		}
	}

	auto d3dDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	// Create a root signature with a single constant buffer slot.
	{

		mRootSignatureDefferedRender.Reset(4);

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			mRootSignatureDefferedRender[0].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			mRootSignatureDefferedRender[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 4, 0);
			mRootSignatureDefferedRender[2].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0);
			mRootSignatureDefferedRender[3].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);
		}

		mRootSignatureDefferedRender.Finalize();
	}

	{
		ID3DBlob* vertexShader = nullptr;
		ID3DBlob* pixelShader = nullptr;
		ID3DBlob* errors = nullptr;

#if defined(_DEBUG)
		// Enable better shader debugging with the graphics debugging tools.
		UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
		UINT compileFlags = 0;
#endif

		std::string path = "";
		char pBuf[1024];

		_getcwd(pBuf, 1024);
		path = pBuf;
		path += "\\";
		std::wstring wpath = std::wstring(path.begin(), path.end());

		std::string vertCompiledPath = path, fragCompiledPath = path;
		vertCompiledPath += "..\\Engine\\assets\\DefferedVS.dxbc";
		fragCompiledPath += "..\\Engine\\assets\\DefferedPS.dxbc";

#define COMPILESHADERS
#ifdef COMPILESHADERS
		std::wstring vertPath = wpath + L"..\\Engine\\assets\\DefferedVS.hlsl";
		std::wstring fragPath = wpath + L"..\\Engine\\assets\\DefferedPS.hlsl";

		try
		{
			DX::ThrowIfFailed(D3DCompileFromFile(vertPath.c_str(), nullptr, nullptr,
				"main", "vs_5_0", compileFlags, 0,
				&vertexShader, &errors));
			DX::ThrowIfFailed(D3DCompileFromFile(fragPath.c_str(), nullptr, nullptr,
				"main", "ps_5_0", compileFlags, 0,
				&pixelShader, &errors));
		}
		catch (std::exception e)
		{
			const char* errStr = (const char*)errors->GetBufferPointer();
			std::cout << errStr;
			errors->Release();
			errors = nullptr;
		}

		std::ofstream vsOut(vertCompiledPath, std::ios::out | std::ios::binary),
			fsOut(fragCompiledPath, std::ios::out | std::ios::binary);

		vsOut.write((const char*)vertexShader->GetBufferPointer(),
			vertexShader->GetBufferSize());
		fsOut.write((const char*)pixelShader->GetBufferPointer(),
			pixelShader->GetBufferSize());

#else
		std::vector<byte> vsBytecodeData = readFile(vertCompiledPath);
		std::vector<byte> fsBytecodeData = readFile(fragCompiledPath);

#endif


		// Create the pipeline state once the shaders are loaded.
		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, position), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, normal), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(Mesh::Vertex, tc), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{ "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, tangent), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{ "BITANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, bitangent), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		};

		D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = mRootSignatureDefferedRender.Get();

#ifdef COMPILESHADERS
		state.VS = CD3DX12_SHADER_BYTECODE(vertexShader->GetBufferPointer(), vertexShader->GetBufferSize());
		state.PS = CD3DX12_SHADER_BYTECODE(pixelShader->GetBufferPointer(), pixelShader->GetBufferSize());
#else
		state.VS = CD3DX12_SHADER_BYTECODE(&m_vertexShader[0], m_vertexShader.size());
		state.PS = CD3DX12_SHADER_BYTECODE(&m_pixelShader[0], m_pixelShader.size());
		vsBytecode.pShaderBytecode = vsBytecodeData.data();
		vsBytecode.BytecodeLength = vsBytecodeData.size();

		psBytecode.pShaderBytecode = fsBytecodeData.data();
		psBytecode.BytecodeLength = fsBytecodeData.size();
#endif

		state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		CD3DX12_DEPTH_STENCIL_DESC DepthStencilState(D3D12_DEFAULT);
		DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_GREATER;
		state.DepthStencilState = DepthStencilState;
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 3;
		state.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
		state.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
		state.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
		state.DSVFormat = DX::DeviceResources::GetInstance().GetDepthBufferFormat();
		state.SampleDesc.Count = 1;

		DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&mPSODefferedRender)));

		// Shader data can be deleted once the pipeline state is created.
		m_vertexShader.clear();
		m_pixelShader.clear();
	}
}

void Renderer::InitLightRenderPipline()
{
	auto d3dDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	// Create a root signature with a single constant buffer slot.
	{

		mRootSignatureLightRender.Reset(3);

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
			mRootSignatureLightRender[0].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_ALL);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
			mRootSignatureLightRender[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);
		}

		{
			CD3DX12_DESCRIPTOR_RANGE range;
			range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 4, 0);
			mRootSignatureLightRender[2].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);
		}

		mRootSignatureLightRender.Finalize();
	}

	{
		ID3DBlob* vertexShader = nullptr;
		ID3DBlob* pixelShader = nullptr;
		ID3DBlob* errors = nullptr;

#if defined(_DEBUG)
		// Enable better shader debugging with the graphics debugging tools.
		UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
		UINT compileFlags = 0;
#endif

		std::string path = "";
		char pBuf[1024];

		_getcwd(pBuf, 1024);
		path = pBuf;
		path += "\\";
		std::wstring wpath = std::wstring(path.begin(), path.end());

		std::string vertCompiledPath = path, fragCompiledPath = path;
		vertCompiledPath += "..\\Engine\\assets\\LightVS.dxbc";
		fragCompiledPath += "..\\Engine\\assets\\LightPS.dxbc";

#define COMPILESHADERS
#ifdef COMPILESHADERS
		std::wstring vertPath = wpath + L"..\\Engine\\assets\\LightVS.hlsl";
		std::wstring fragPath = wpath + L"..\\Engine\\assets\\LightPS.hlsl";

		try
		{
			DX::ThrowIfFailed(D3DCompileFromFile(vertPath.c_str(), nullptr, nullptr,
				"main", "vs_5_0", compileFlags, 0,
				&vertexShader, &errors));
			DX::ThrowIfFailed(D3DCompileFromFile(fragPath.c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE,
				"main", "ps_5_0", compileFlags, 0,
				&pixelShader, &errors));
		}
		catch (std::exception e)
		{
			const char* errStr = (const char*)errors->GetBufferPointer();
			std::cout << errStr;
			errors->Release();
			errors = nullptr;
		}

		std::ofstream vsOut(vertCompiledPath, std::ios::out | std::ios::binary),
			fsOut(fragCompiledPath, std::ios::out | std::ios::binary);

		vsOut.write((const char*)vertexShader->GetBufferPointer(),
			vertexShader->GetBufferSize());
		fsOut.write((const char*)pixelShader->GetBufferPointer(),
			pixelShader->GetBufferSize());

#else
		std::vector<byte> vsBytecodeData = readFile(vertCompiledPath);
		std::vector<byte> fsBytecodeData = readFile(fragCompiledPath);

#endif


		// Create the pipeline state once the shaders are loaded.
		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, position), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Mesh::Vertex, normal), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		};

		D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = mRootSignatureLightRender.Get();

#ifdef COMPILESHADERS
		state.VS = CD3DX12_SHADER_BYTECODE(vertexShader->GetBufferPointer(), vertexShader->GetBufferSize());
		state.PS = CD3DX12_SHADER_BYTECODE(pixelShader->GetBufferPointer(), pixelShader->GetBufferSize());
#else
		state.VS = CD3DX12_SHADER_BYTECODE(&m_vertexShader[0], m_vertexShader.size());
		state.PS = CD3DX12_SHADER_BYTECODE(&m_pixelShader[0], m_pixelShader.size());
		vsBytecode.pShaderBytecode = vsBytecodeData.data();
		vsBytecode.BytecodeLength = vsBytecodeData.size();

		psBytecode.pShaderBytecode = fsBytecodeData.data();
		psBytecode.BytecodeLength = fsBytecodeData.size();
#endif
		CD3DX12_RASTERIZER_DESC rasterizerState(D3D12_DEFAULT);
		rasterizerState.CullMode = D3D12_CULL_MODE_FRONT;
		state.RasterizerState = rasterizerState;
		state.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
		CD3DX12_DEPTH_STENCIL_DESC depthStencilState(D3D12_DEFAULT);
		depthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
		depthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
		state.DepthStencilState = depthStencilState;
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 1;
		state.RTVFormats[0] = DX::DeviceResources::GetInstance().GetBackBufferFormat();
		state.DSVFormat = DX::DeviceResources::GetInstance().GetDepthBufferFormat();
		state.SampleDesc.Count = 1;

		DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&mPSOLightRender)));

		// Shader data can be deleted once the pipeline state is created.
		m_vertexShader.clear();
		m_pixelShader.clear();
	}
}

void Renderer::CopyDepthBuffer()
{
	CD3DX12_RESOURCE_BARRIER depthResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(DX::DeviceResources::GetInstance().GetDepthStencil(), D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_COPY_SOURCE);
	mCommandList->ResourceBarrier(1, &depthResourceBarrier);

	CD3DX12_RESOURCE_BARRIER gBufferDepthResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(mGBufferResourses[DX::DeviceResources::GetInstance().GetCurrentFrameIndex()].Depth.GetResource(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	mCommandList->ResourceBarrier(1, &gBufferDepthResourceBarrier);

	mCommandList->CopyResource(mGBufferResourses[DX::DeviceResources::GetInstance().GetCurrentFrameIndex()].Depth.GetResource(), DX::DeviceResources::GetInstance().GetDepthStencil());

	CD3DX12_RESOURCE_BARRIER postDepthResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(DX::DeviceResources::GetInstance().GetDepthStencil(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);
	mCommandList->ResourceBarrier(1, &postDepthResourceBarrier);

	CD3DX12_RESOURCE_BARRIER postGBufferDepthResourceBarrier =
		CD3DX12_RESOURCE_BARRIER::Transition(mGBufferResourses[DX::DeviceResources::GetInstance().GetCurrentFrameIndex()].Depth.GetResource(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	mCommandList->ResourceBarrier(1, &postGBufferDepthResourceBarrier);
}

