#include "pch.h"
#include "SolidBuffer.h"
#include "../DeviceResources.h"

void SolidBuffer::Init(DescriptorHeap* descriptorHeap, uint32_t offsetInDescriptors, size_t lenghtSolidBuffer)
{
	mDescriptorHeap = descriptorHeap;
	mDescriptorSize = descriptorHeap->GetDescriptorSize();
	mOffsetInDescriptors = offsetInDescriptors;
	mLenghtSolidBuffer = lenghtSolidBuffer;
}

engine::ID SolidBuffer::Add(ID3D12GraphicsCommandList* commandList, ID3D12PipelineState* pipelineState, OpaqueTextures& textures)
{
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetCommandAllocator()->Reset());

	// The command list can be reset anytime after ExecuteCommandList() is called.
	DX::ThrowIfFailed(commandList->Reset(DX::DeviceResources::GetInstance().GetCommandAllocator(), pipelineState));
	
	uint32_t posInDescriptorHeap = mOffsetInDescriptors + mTextures.getNextUnused() * 4;

	AddTexture(textures.albedo->GetResource(), posInDescriptorHeap);
	AddTexture(textures.normal->GetResource(), posInDescriptorHeap + 1);
	AddTexture(textures.roughness->GetResource(), posInDescriptorHeap + 2);
	AddTexture(textures.metalness->GetResource(), posInDescriptorHeap + 3);

	DX::DeviceResources::GetInstance().ExecuteCommandList(commandList);

	engine::ID returnId = mTextures.insert(CD3DX12_GPU_DESCRIPTOR_HANDLE(mDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), posInDescriptorHeap, mDescriptorSize));
	textures.idInDescriptorHeap = returnId;
	return returnId;
}

void SolidBuffer::AddTexture(ID3D12Resource* resource, uint32_t offset)
{
	D3D12_RESOURCE_DESC textDesc = resource->GetDesc();

	D3D12_SHADER_RESOURCE_VIEW_DESC desc{};
	desc.Format = textDesc.Format;
	desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MipLevels = textDesc.MipLevels;
	desc.Texture2D.MostDetailedMip = 0;
	desc.Texture2D.PlaneSlice = 0;
	desc.Texture2D.ResourceMinLODClamp = 0.0f;
	desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	mDescriptorHeap->SetShaderResourseView(resource, desc, offset);
}
