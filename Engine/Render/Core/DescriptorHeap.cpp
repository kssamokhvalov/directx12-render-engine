#include "pch.h"

#include "DescriptorHeap.h"
#include "../DeviceResources.h"
#include "GpuBufferHeap.h"

void DescriptorHeap::Create(D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flag)
{
	mHeapDesc = {};
	mHeapDesc.NumDescriptors = numDescriptors;
	mHeapDesc.Type = type;
	mHeapDesc.Flags = flag;
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateDescriptorHeap(&mHeapDesc, IID_PPV_ARGS(&mDescriptorHeap)));

	mCPUDescriptorHandleForHeapStart = mDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	mGPUDescriptorHandleForHeapStart = mDescriptorHeap->GetGPUDescriptorHandleForHeapStart();

	mDescriptorSize = DX::DeviceResources::GetInstance().GetD3DDevice()->GetDescriptorHandleIncrementSize(mHeapDesc.Type);
}

void DescriptorHeap::SetConstantBufferView(D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc, uint32_t index)
{
	UINT descriptorSize = DX::DeviceResources::GetInstance().GetD3DDevice()->GetDescriptorHandleIncrementSize(mHeapDesc.Type);
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(mCPUDescriptorHandleForHeapStart);
	cpuHandle.Offset(descriptorSize * index);
	DX::DeviceResources::GetInstance().GetD3DDevice()->CreateConstantBufferView(&cbvDesc, cpuHandle);
}

void DescriptorHeap::SetConstantBufferViews(uint32_t offset, uint32_t numViews, D3D12_GPU_VIRTUAL_ADDRESS bufferLocation, UINT sizeInBytes)
{
	for (int i = offset; i < offset + numViews; i++)
	{
		D3D12_CONSTANT_BUFFER_VIEW_DESC desc;
		desc.BufferLocation = bufferLocation;
		desc.SizeInBytes = sizeInBytes;
		SetConstantBufferView(desc, i);

		bufferLocation += desc.SizeInBytes;
	}
}

void DescriptorHeap::SetRenderTargetView(ID3D12Resource* resource, D3D12_RENDER_TARGET_VIEW_DESC& rtvDesc, uint32_t index)
{
	UINT descriptorSize = DX::DeviceResources::GetInstance().GetD3DDevice()->GetDescriptorHandleIncrementSize(mHeapDesc.Type);
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(mCPUDescriptorHandleForHeapStart);
	cpuHandle.Offset(descriptorSize * index);
	DX::DeviceResources::GetInstance().GetD3DDevice()->CreateRenderTargetView(resource, nullptr, cpuHandle);
}

void DescriptorHeap::SetDepthStencilView(ID3D12Resource* resource, D3D12_DEPTH_STENCIL_VIEW_DESC& dsvDesc, uint32_t index)
{
	UINT descriptorSize = DX::DeviceResources::GetInstance().GetD3DDevice()->GetDescriptorHandleIncrementSize(mHeapDesc.Type);
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(mCPUDescriptorHandleForHeapStart);
	cpuHandle.Offset(descriptorSize * index);
	DX::DeviceResources::GetInstance().GetD3DDevice()->CreateDepthStencilView(resource, nullptr, cpuHandle);
}

void DescriptorHeap::SetShaderResourseView(ID3D12Resource* resource, D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc, uint32_t index)
{
	UINT descriptorSize = DX::DeviceResources::GetInstance().GetD3DDevice()->GetDescriptorHandleIncrementSize(mHeapDesc.Type);
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(mCPUDescriptorHandleForHeapStart);
	cpuHandle.Offset(descriptorSize * index);
	DX::DeviceResources::GetInstance().GetD3DDevice()->CreateShaderResourceView(resource, &srvDesc, cpuHandle);
}

void DescriptorHeap::SetTextureAsShaderResourseView(ID3D12Resource* resource, uint32_t index)
{
	D3D12_RESOURCE_DESC textDesc = resource->GetDesc();

	D3D12_SHADER_RESOURCE_VIEW_DESC desc{};
	desc.Format = textDesc.Format;
	desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MipLevels = textDesc.MipLevels;
	desc.Texture2D.MostDetailedMip = 0;
	desc.Texture2D.PlaneSlice = 0;
	desc.Texture2D.ResourceMinLODClamp = 0.0f;
	desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	SetShaderResourseView(resource, desc, index);
}

void DescriptorHeap::SetTextureAsShaderResourseView(ID3D12Resource* resource, DXGI_FORMAT format, uint32_t index)
{
	D3D12_RESOURCE_DESC textDesc = resource->GetDesc();

	D3D12_SHADER_RESOURCE_VIEW_DESC desc{};
	desc.Format = format;
	desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MipLevels = textDesc.MipLevels;
	desc.Texture2D.MostDetailedMip = 0;
	desc.Texture2D.PlaneSlice = 0;
	desc.Texture2D.ResourceMinLODClamp = 0.0f;
	desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	SetShaderResourseView(resource, desc, index);
}

void DescriptorHeap::SetSampler(D3D12_SAMPLER_DESC& samplerDesc)
{
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(mCPUDescriptorHandleForHeapStart);
	DX::DeviceResources::GetInstance().GetD3DDevice()->CreateSampler(&samplerDesc, cpuHandle);

}
