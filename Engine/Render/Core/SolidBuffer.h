#pragma once
#include "../../Utils/SolidVector.h"
#include "DescriptorHeap.h"
#include "Texture.h"

#include "../../Utils/ThirdsLibs/headers/DirectXTK12/d3dx12.h"

struct GBuffer;

class SolidBuffer
{
public:
	void Init(DescriptorHeap* descriptorHeap, uint32_t offsetInDescriptors, size_t	lenghtSolidBuffer);
	engine::ID Add(ID3D12GraphicsCommandList* commandList, ID3D12PipelineState* pipelineState, OpaqueTextures& textures);
	CD3DX12_GPU_DESCRIPTOR_HANDLE operator[](int i) { return mTextures.at(i); }
private:
	void AddTexture(ID3D12Resource* resource, uint32_t offset);
private:
	SolidVector<CD3DX12_GPU_DESCRIPTOR_HANDLE>		mTextures;
	size_t											mLenghtSolidBuffer;

	DescriptorHeap*									mDescriptorHeap;
	uint32_t										mDescriptorSize;
	uint32_t										mOffsetInDescriptors;
};

