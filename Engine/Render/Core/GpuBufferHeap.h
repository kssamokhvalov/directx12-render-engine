#pragma once
#include "GpuResource.h"

#include <stdint.h>

class GpuBufferHeap : public GpuResource
{
public:
	void Create(uint32_t numElements, uint32_t elementSize, void* data);
	void Create(uint64_t bufferSize, void* data);
	void* GetData() { return mData; }
private:
	uint32_t		mNumElements;
	uint32_t		mElementSize;
	uint64_t		mBufferSize;
	void*			mData;
};

class ConstantBuffer : public GpuResource
{
public:
	void Create(uint32_t numBuffers, uint32_t bufferSize);
	void Resize(uint32_t bufferSize);
	uint32_t GetFullSize() const { return mBufferSize * mNumBuffers; }
	uint32_t GetBufferSize() const { return mBufferSize; }
	uint32_t GetNumBuffer() const { return mNumBuffers; }
	void Map(UINT subresource, const D3D12_RANGE* readRange, void** data);
	void Unmap(UINT subresource, const D3D12_RANGE* writtenRange);
private:
	uint32_t		mBufferSize;
	uint32_t		mNumBuffers;
};

