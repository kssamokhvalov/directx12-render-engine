#pragma once
#include "GpuBufferHeap.h"
#include "../../Utils/SolidVector.h"


#include <memory>
#include <vector>



class Texture : public GpuResource
{
public:

	void CreateTextureForRT(D3D12_RESOURCE_DESC& textureDesc);
	void CreateTexture(D3D12_RESOURCE_DESC& textureDesc);
	void CreateDepthTexture(D3D12_RESOURCE_DESC& textureDesc);

	std::unique_ptr<uint8_t[]>& GetDDSData() { return mDDSData; }
	std::vector<D3D12_SUBRESOURCE_DATA>& GetSubresources() { return mSubresources; }
	GpuBufferHeap* GetTextureHeap() { return &mTextureHeap; }

	void SetState(D3D12_RESOURCE_STATES state) { mCurrentState = state; }
	D3D12_RESOURCE_STATES GetCurrentState() const { return mCurrentState; }
private:
	std::unique_ptr<uint8_t[]>				mDDSData;
	std::vector<D3D12_SUBRESOURCE_DATA>		mSubresources;

	D3D12_RESOURCE_STATES					mCurrentState;

	GpuBufferHeap							mTextureHeap;
};

enum HasTextures
{
	Albedo = 1,
	Normal = 1 << 1,
	Roughness = 1 << 2,
	Metalness = 1 << 3
};

struct OpaqueTextures
{
	Texture* albedo;
	Texture* normal;
	Texture* roughness;
	Texture* metalness;
	engine::ID idInDescriptorHeap;
	int32_t	hasTextures;
};