#pragma once

#include "../../ThirdsLibs/headers/DirectXTK12/d3dx12.h"

#include <d3d12.h>
#include <wrl/client.h>
#include <stdint.h>

class ConstantBuffer;

class DescriptorHeap
{
public:
	void Create(D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t numDescriptors = 256, D3D12_DESCRIPTOR_HEAP_FLAGS flag = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
	void SetConstantBufferView(D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc, uint32_t index);
	void SetConstantBufferViews( uint32_t offset, uint32_t numViews, D3D12_GPU_VIRTUAL_ADDRESS bufferLocation, UINT sizeInBytes);
	void SetRenderTargetView(ID3D12Resource* resource, D3D12_RENDER_TARGET_VIEW_DESC& rtvDesc, uint32_t index);
	void SetDepthStencilView(ID3D12Resource* resource, D3D12_DEPTH_STENCIL_VIEW_DESC& dsvDesc, uint32_t index);

	void SetShaderResourseView(ID3D12Resource* resource, D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc, uint32_t index);
	void SetTextureAsShaderResourseView(ID3D12Resource* resource, uint32_t index);
	void SetTextureAsShaderResourseView(ID3D12Resource* resource, DXGI_FORMAT format, uint32_t index);
	void SetSampler(D3D12_SAMPLER_DESC& samplerDesc);

	ID3D12DescriptorHeap* GetDescriptorHeap() { return mDescriptorHeap.Get(); }
	ID3D12DescriptorHeap** GetAddressOf() { return mDescriptorHeap.GetAddressOf(); }

	UINT GetDescriptorSize() { return mDescriptorSize; }

	CD3DX12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart() { return mCPUDescriptorHandleForHeapStart; }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapStart() { return mGPUDescriptorHandleForHeapStart; }
private:
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>	mDescriptorHeap;
	CD3DX12_CPU_DESCRIPTOR_HANDLE					mCPUDescriptorHandleForHeapStart;
	D3D12_GPU_DESCRIPTOR_HANDLE						mGPUDescriptorHandleForHeapStart;
	D3D12_DESCRIPTOR_HEAP_DESC						mHeapDesc;
	UINT											mDescriptorSize;
};

