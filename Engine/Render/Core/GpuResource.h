#pragma once

#include <d3d12.h>
#include <wrl/client.h>

class GpuResource
{
public:
	ID3D12Resource* GetResource() { return mResource.Get(); }
	ID3D12Resource** GetAddressOf() { return mResource.GetAddressOf(); }

	D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress() const { return mGpuVirtualAddress; }

	D3D12_RESOURCE_STATES GetUsageState() { return mUsageState; }
protected:
	Microsoft::WRL::ComPtr<ID3D12Resource>	mResource;
	D3D12_RESOURCE_STATES					mUsageState;
	D3D12_GPU_VIRTUAL_ADDRESS				mGpuVirtualAddress;
};

