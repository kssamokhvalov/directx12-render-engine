#include "pch.h"
#include "Texture.h"
#include "../DeviceResources.h"

void Texture::CreateTextureForRT(D3D12_RESOURCE_DESC& textureDesc)
{
	ID3D12Device* pDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	D3D12_HEAP_PROPERTIES textureHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	CD3DX12_CLEAR_VALUE optimizedClearValue(textureDesc.Format, DirectX::Colors::Black);

	DX::ThrowIfFailed(pDevice->CreateCommittedResource(
		&textureHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&textureDesc,
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		&optimizedClearValue,
		IID_PPV_ARGS(&mResource)
	));
}

void Texture::CreateTexture(D3D12_RESOURCE_DESC& textureDesc)
{
	ID3D12Device* pDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	D3D12_HEAP_PROPERTIES textureHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	DX::ThrowIfFailed(pDevice->CreateCommittedResource(
		&textureHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&textureDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&mResource)
	));
}

void Texture::CreateDepthTexture(D3D12_RESOURCE_DESC& textureDesc)
{
	ID3D12Device* pDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	D3D12_HEAP_PROPERTIES textureHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	CD3DX12_CLEAR_VALUE depthOptimizedClearValue(textureDesc.Format, 1.0f, 0);

	DX::ThrowIfFailed(pDevice->CreateCommittedResource(
		&textureHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&textureDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&depthOptimizedClearValue,
		IID_PPV_ARGS(&mResource)
	));
}
