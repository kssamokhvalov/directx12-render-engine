#include "pch.h"
#include "GpuBuffer.h"
#include "../DeviceResources.h"
#include "../DirectXHelper.h"

#include <direct.h>


void VertexBuffer::Create(uint32_t numElements, uint32_t elementSize)
{
	mNumElements = numElements;
	mElementSize = elementSize;
	mBufferSize = numElements * elementSize;
	mUsageState = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize);
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

D3D12_VERTEX_BUFFER_VIEW VertexBuffer::GetVertexBufferView()
{
	D3D12_VERTEX_BUFFER_VIEW tmp{};

	tmp.BufferLocation = mGpuVirtualAddress;
	tmp.StrideInBytes = mElementSize;
	tmp.SizeInBytes = mBufferSize;

	return tmp;
}

void IndexBuffer::Create(uint32_t numElements, uint32_t elementSize)
{
	mNumElements = numElements;
	mElementSize = elementSize;
	mBufferSize = numElements * elementSize;
	mFormat = DXGI_FORMAT_R32_UINT;
	mUsageState = D3D12_RESOURCE_STATE_INDEX_BUFFER;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize);
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

D3D12_INDEX_BUFFER_VIEW IndexBuffer::GetIndexBufferView()
{
	D3D12_INDEX_BUFFER_VIEW tmp{};

	tmp.BufferLocation = mGpuVirtualAddress;
	tmp.SizeInBytes = mBufferSize;
	tmp.Format = mFormat;

	return tmp;
}
