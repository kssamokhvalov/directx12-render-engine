#pragma once

#include "../../ThirdsLibs/headers/DirectXTK12/d3dx12.h"

#include <d3d12.h>
#include <wrl/client.h>
#include<vector>

class RootSignature
{
public:
	void Finalize();
	void Reset(size_t numParametrs) { mRootParametrs.resize(numParametrs); }
	CD3DX12_ROOT_PARAMETER& operator[](size_t index) { return mRootParametrs[index]; }

	ID3D12RootSignature* Get() { return mRootSignature.Get(); }
private:
	Microsoft::WRL::ComPtr<ID3D12RootSignature>	mRootSignature;
	std::vector<CD3DX12_ROOT_PARAMETER>			mRootParametrs;
};

