#pragma once
#include "GpuBufferHeap.h"

struct CD3DX12_GPU_DESCRIPTOR_HANDLE;

class RingBuffer
{
public:
	void Init(uint32_t bufferSize, size_t bufferLenght = 256);
	void Deinit();
	void SetDescriptorHeapDesc(D3D12_GPU_DESCRIPTOR_HANDLE gpuDescriptorHandleForHeapStart, uint32_t descriptorSize, uint32_t offsetInDescriptors);
	
	void Push(void const* src,	size_t size);
	CD3DX12_GPU_DESCRIPTOR_HANDLE Pop();

	size_t GetLenghtBuffer() const { return mLenghtRingBuffer; }
	D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress() const { return mBuffer.GetGPUVirtualAddress(); }
	uint32_t GetBufferSize() const { return mBufferSize; }
private:
	ConstantBuffer					mBuffer;
	uint8_t*						mMappedBuffer;

	uint32_t						mBeginIterator;
	uint32_t						mEndIterator;

	size_t							mLenghtRingBuffer;
	uint32_t						mBufferSize;

	D3D12_GPU_DESCRIPTOR_HANDLE		mGPUDescriptorHandleForHeapStart;
	uint32_t						mDescriptorSize;
	uint32_t						mOffsetInDescriptors;
};