#include "pch.h"
#include "RingBuffer.h"

#include "../../ThirdsLibs/headers/DirectXTK12/d3dx12.h"


void RingBuffer::Init(uint32_t bufferSize, size_t bufferLenght)
{
	mLenghtRingBuffer = bufferLenght;
	mBufferSize = bufferSize;

	mBuffer.Create(mLenghtRingBuffer, mBufferSize);

	CD3DX12_RANGE readRange(0, 0);
	mBuffer.Map(0, &readRange, reinterpret_cast<void**>(&mMappedBuffer));
}

void RingBuffer::Deinit()
{
	mBuffer.Unmap(0, nullptr);
}

void RingBuffer::SetDescriptorHeapDesc(D3D12_GPU_DESCRIPTOR_HANDLE gpuDescriptorHandleForHeapStart, uint32_t descriptorSize, uint32_t offsetInDescriptors)
{
	mDescriptorSize = descriptorSize;
	mOffsetInDescriptors = offsetInDescriptors;
	mGPUDescriptorHandleForHeapStart = gpuDescriptorHandleForHeapStart;
}

void RingBuffer::Push(void const* src, size_t size)
{
	uint32_t numElements = size / mBufferSize;

	if (mEndIterator + numElements < mLenghtRingBuffer)
	{
		uint8_t* destination = mMappedBuffer + mEndIterator * mBufferSize;
		CopyMemory(destination, src, size);
		mEndIterator += numElements;
	}
	else
	{
		uint32_t sizePartBuffer = mLenghtRingBuffer - mEndIterator;
		uint8_t* destination = mMappedBuffer + mEndIterator * mBufferSize;
		CopyMemory(destination, src, sizePartBuffer * mBufferSize);
		src = reinterpret_cast<const void*> (reinterpret_cast<const uint8_t*>(src) + sizePartBuffer * mBufferSize);
		CopyMemory(mMappedBuffer, src, (size - sizePartBuffer * mBufferSize));


		mEndIterator = numElements - sizePartBuffer;
	}
}

CD3DX12_GPU_DESCRIPTOR_HANDLE RingBuffer::Pop()
{
	uint32_t tmpIterator = mBeginIterator;

	mBeginIterator = mBeginIterator + 1 == mLenghtRingBuffer ? 0 : mBeginIterator + 1;

	return CD3DX12_GPU_DESCRIPTOR_HANDLE(mGPUDescriptorHandleForHeapStart, mOffsetInDescriptors + tmpIterator, mDescriptorSize);
}
