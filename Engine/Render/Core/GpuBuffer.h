#pragma once

#include "GpuResource.h"

#include <stdint.h>

class GpuBuffer : public GpuResource
{
public:
	uint32_t GetBufferSize() const { return mBufferSize; }
protected:
	uint32_t	mNumElements;
	uint32_t	mElementSize;
	uint32_t	mBufferSize;
};

class VertexBuffer : public GpuBuffer
{
public:
	void Create(uint32_t numElements, uint32_t elementSize);
	D3D12_VERTEX_BUFFER_VIEW GetVertexBufferView();
};

class IndexBuffer : public GpuBuffer
{
public:
	void Create(uint32_t numElements, uint32_t elementSize);
	D3D12_INDEX_BUFFER_VIEW GetIndexBufferView();
private:
	DXGI_FORMAT mFormat;
};
