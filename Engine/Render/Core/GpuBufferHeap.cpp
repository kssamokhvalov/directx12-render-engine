#include "pch.h"
#include "GpuBufferHeap.h"
#include "../DeviceResources.h"

#include <direct.h>

void GpuBufferHeap::Create(uint32_t numElements, uint32_t elementSize, void* data)
{
	mNumElements = numElements;
	mElementSize = elementSize;
	mBufferSize = numElements * elementSize;
	mData = data;

	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize);
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

void GpuBufferHeap::Create(uint64_t bufferSize, void* data)
{
	mBufferSize = bufferSize;
	mData = data;

	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize);
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

void ConstantBuffer::Create(uint32_t numBuffers, uint32_t bufferSize)
{
	mNumBuffers = numBuffers;
	mBufferSize = bufferSize;
	CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize * mNumBuffers);
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&constantBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

void ConstantBuffer::Resize(uint32_t bufferSize)
{
	mBufferSize = bufferSize;
	CD3DX12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize * mNumBuffers);
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	mResource.Reset();
	DX::ThrowIfFailed(DX::DeviceResources::GetInstance().GetD3DDevice()->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&constantBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&mResource)));

	mGpuVirtualAddress = mResource->GetGPUVirtualAddress();
}

void ConstantBuffer::Map(UINT subresource, const D3D12_RANGE* readRange, void** data)
{
	mResource->Map(subresource, readRange, data);
}

void ConstantBuffer::Unmap(UINT subresource, const D3D12_RANGE* writtenRange)
{
	mResource->Unmap(subresource, writtenRange);
}

