#include "pch.h"
#include "Camera.h"

using namespace DirectX;
void Camera::AddRelativePosition(const DirectX::XMVECTOR& offset)
{
	XMVECTOR worldOffset = DirectX::XMVector4Transform(offset, mViewInv);
	mPosition = DirectX::XMVectorAdd(mPosition, worldOffset);
}

void Camera::AddRelativeAngles(const DirectX::XMVECTOR& rotation)
{
    if (mFixedBottom)
    {
        mRotation = DirectX::XMQuaternionMultiply(mRotation, DirectX::XMQuaternionRotationAxis(Right(), rotation.m128_f32[1]));                                             // rotate around Camera's X
        mRotation = DirectX::XMQuaternionMultiply(mRotation, DirectX::XMQuaternionRotationAxis(DirectX::XMVectorSet(0.f, 1.f, 0.f, 0.f), rotation.m128_f32[2]));            // rotate around World's Y
    }
}

void Camera::UpdateMatrix()
{
    mViewInv = XMMatrixAffineTransformation(DirectX::XMVectorSet(1.f, 1.f, 1.f, 0.f), XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f), mRotation, mPosition);
    mView = XMMatrixInverse(nullptr, mViewInv);
}
