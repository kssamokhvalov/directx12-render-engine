#pragma once

#include "../Utils/Singlton.h"

#include <unordered_map>
#include <string>
#include <array>
#include <memory>

struct Model;



class ModelManager : public Singlton<ModelManager>
{
public:
	void Init();
	void Deinit();

	Model* GetUnitSphere();
	Model* GetModel(const std::string& path);
private:
	Model* AssimpLoader(const std::string& path);
	std::unordered_map<std::string, std::shared_ptr<Model>> mLoadedModels;

	Model* InitUnitSphere();

private:
	enum DefaultModels
	{
		UNIT_CUBE = 0,
		UNIT_SPHERE,
		UNIT_SPHERE_FLAT
	};

	std::array<std::string, 2> mDefaultModels =
	{
		"unitCube",
		"unitSphere"
	};
};

