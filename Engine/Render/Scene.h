#pragma once

#include "Core/Texture.h"

#include <entt/entt.hpp>

#include <DirectXMath.h>
#include <vector>

struct Model;

enum class Renderable
{
	Opaque
};


struct TransformComponent
{
	DirectX::XMFLOAT4X4 transform;
};

struct ModelComponent
{
	Model* model;
	std::vector<OpaqueTextures> textures;
};

struct LightComponent
{
	DirectX::XMFLOAT3	color;
	float				intensity;
	float				radiusLight;
	float				influenceDistance;
};


class Scene
{
public:
	void AddModelsToRender();
	void AddLightsToRender();
	void AddSphereLight(const DirectX::XMFLOAT4X4& transform, DirectX::XMFLOAT3 color);
	void AddSphereLight(const DirectX::XMFLOAT4X4& transform, LightComponent lightComponent);
	void AddModel(ModelComponent modelComponent, const DirectX::XMFLOAT4X4& transform);
private:
	entt::registry mRegistry;
};

