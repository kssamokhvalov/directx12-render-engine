#include "pch.h"
#include "TextureManager.h"
#include "DeviceResources.h"

#include "../Utils/ThirdsLibs/headers/DirectXTK12/DDSTextureLoader12.h"

Texture* TextureManager::GetTexture(const std::string& path)
{
	auto search = mLoadedTextures.find(path);
	if (search != mLoadedTextures.end())
		return search->second.get();
	else
		return DDSTextureLoader(path);
}

Texture* TextureManager::DDSTextureLoader(const std::string& path)
{
	std::wstring wpath = std::wstring(path.begin(), path.end());
	std::unique_ptr<Texture> texture(new Texture);

	ID3D12Device* d3dDevice = DX::DeviceResources::GetInstance().GetD3DDevice();

	DirectX::LoadDDSTextureFromFile(d3dDevice, wpath.c_str(), texture->GetAddressOf(), texture->GetDDSData(), texture->GetSubresources());

	D3D12_RESOURCE_DESC textDesc = texture->GetResource()->GetDesc();
	uint8_t* data = texture->GetDDSData().get();

	const UINT num2DSubresources = textDesc.DepthOrArraySize * textDesc.MipLevels;
	const UINT64 uploadBufferSize = GetRequiredIntermediateSize(texture->GetResource(), 0, num2DSubresources);
	texture->GetTextureHeap()->Create(uploadBufferSize, reinterpret_cast<void*>(data));

	texture->SetState(D3D12_RESOURCE_STATE_COMMON);

	Texture* tmpPtr = texture.get();

	mLoadedTextures.emplace(path, std::move(texture));
	return tmpPtr;
}
