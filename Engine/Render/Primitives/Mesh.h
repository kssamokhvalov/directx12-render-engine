#pragma once

#include "../Core/GpuBuffer.h"
#include "../Core/GpuBufferHeap.h"

#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <vector>
#include <string>


struct Face
{
	uint32_t indexs[3];
};

struct Mesh
{
	struct Vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 tc;
		DirectX::XMFLOAT3 tangent;
		DirectX::XMFLOAT3 bitangent;
	};

	bool Init()
	{
		mVertexBuffer.Create(mVertices.size(), sizeof(Vertex));
		mIndexBuffer.Create(mFaces.size() * 3, sizeof(mFaces[0].indexs[0]));

		mVertexBufferView = mVertexBuffer.GetVertexBufferView();
		mIndexBufferView = mIndexBuffer.GetIndexBufferView();

		mVertexBufferUpload.Create(mVertices.size(), sizeof(Vertex), mVertices.data());
		mIndexBufferUpload.Create(mFaces.size() * 3, sizeof(mFaces[0].indexs[0]), mFaces.data());

		return true;
	}

	std::vector<Face>					mFaces;
	std::vector<Vertex>					mVertices;
	std::vector<DirectX::XMFLOAT4X4>	mInstances;
	std::vector<DirectX::XMFLOAT4X4>	mInstancesInv;
	std::string							mName;
	DirectX::BoundingBox				mBoundingBox;

	VertexBuffer						mVertexBuffer;
	IndexBuffer							mIndexBuffer;
	D3D12_VERTEX_BUFFER_VIEW			mVertexBufferView;
	D3D12_INDEX_BUFFER_VIEW				mIndexBufferView;
	GpuBufferHeap						mVertexBufferUpload;
	GpuBufferHeap						mIndexBufferUpload;
};