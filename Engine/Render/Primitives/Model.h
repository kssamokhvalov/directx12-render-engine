#pragma once
#include "Mesh.h"

struct Model
{
	std::string				mName;
	DirectX::BoundingBox	mBoundingBox;
	std::vector<Mesh>		mMeshes;
};