cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	uint width;
	uint height;
	float rWidth;
	float rHeight;
	matrix view;
	matrix projection;
};

cbuffer LightConstantBuffer : register(b1)
{
	matrix	light;
	float3	I;
	float radiusLight;
	float influenceDistance;
};

struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	 
};

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 I : COLOR;
	nointerpolation float3 lightPos : LIGHT_POS;
	nointerpolation float radiusLight : LIGTH_RADIUS;
	nointerpolation float influenceDistance : INFL_DISTANCE;
};

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);

	pos = mul(pos, light);
	pos = mul(pos, view);
	pos = mul(pos, projection);

	output.pos = pos;
	output.I = I;
	output.lightPos = light._41_42_43;
	output.influenceDistance = influenceDistance;

	return output;
}