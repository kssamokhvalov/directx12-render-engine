static const int HAS_ALBEDO = 1;
static const int HAS_NORMAL = 1 << 1;
static const int HAS_ROUGHNESS = 1 << 2;
static const int HAS_METALNESS = 1 << 3;

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 tc : TEXCOORD;
	float3 tangent : TANGENT;
	float3 bitangent : BITANGENT;
	int	material : MATERIAL;
};

Texture2D<float4> albedo : register(t0);
Texture2D<float4> normal : register(t1);
Texture2D<float4> roughness : register(t2);
Texture2D<float4> metalness : register(t3);

SamplerState samplerLinearWarp : register(s0);

struct PixelShaderOutput
{
	float4 albedoColor : SV_Target0;
	float4 normal : SV_Target1;
};

PixelShaderOutput main(PixelShaderInput input) : SV_TARGET
{
	PixelShaderOutput output;
	output.albedoColor = albedo.Sample(samplerLinearWarp, input.tc);

	if (input.material & HAS_NORMAL)
	{
		float3 N = normal.Sample(samplerLinearWarp, input.tc).xyz;
		N = N / length(N);
		float3x3 TBN = float3x3(input.tangent, input.bitangent, input.normal);
		N = N * 2.0f - 1.0f;
		output.normal = float4(mul(N, TBN), 1.0f);
	}
	else
	{
		output.normal = float4(input.normal, 1.0f);
	}
	
	return output;
}
