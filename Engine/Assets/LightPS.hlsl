#include "PBR.hlsli"

cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	uint width;
	uint height;
	float rWidth;
	float rHeight;
	matrix view;
	matrix projection;
	matrix projViewInv;
};

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 I : COLOR;
	nointerpolation float3 lightPos : LIGHT_POS;
	nointerpolation float radiusLight : LIGTH_RADIUS;
	nointerpolation float influenceDistance : INFL_DISTANCE;
};

Texture2D<float4> GBuffer0 : register(t0);
Texture2D<float4> GBuffer1 : register(t1);
Texture2D<float4> GBuffer2 : register(t2);
Texture2D<float> Depth : register(t3);

static const float PI = 3.14159265f;

float4 main(PixelShaderInput input) : SV_TARGET
{
	float2 screenTC = float2(input.pos.x * rWidth, input.pos.y * rHeight);
	float2 screenPos = screenTC * 2.0f - 1.0f;
	screenPos.y *= -1; //swap y
	float z = Depth.Load(int3(input.pos.xy, 0));

	float4 pos = mul(float4(screenPos, z, 1.0f), projViewInv);
	pos = pos / pos.w;

	float4 baseColor = GBuffer0.Load(int3(input.pos.xy, 0));

	float3 f0 = float3(1.0f, 1.0f, 1.0f);
	float roughness = 1.0f;
	float linearRoughness = 1.0f;

	float3 camPos = view._41_42_43;
	float3 V = camPos - pos.xyz;
	float3 L = input.lightPos - pos.xyz;
	float3 N = GBuffer1.Load(int3(input.pos.xy, 0)).xyz;

	// This code is an example of call of previous functions
	float NdotV = abs(dot(N, V)) + 1e-5f; // avoid artifact
	float3 H = normalize(V + L);
	float LdotH = saturate(dot(L, H));
	float NdotH = saturate(dot(N, H));
	float NdotL = saturate(dot(N, L));
	
	// Specular BRDF
	float3 F = fresnel(LdotH, f0);
	float Vis = V_SmithGGXCorrelated(NdotV, NdotL, roughness);
	float D = D_GGX(NdotH, roughness);
	float Fr = D * F * Vis / PI;

	// Diffuse BRDF
	float Fd = Fr_DisneyDiffuse(NdotV, NdotL, LdotH, linearRoughness) / PI;


	return float4(baseColor.rgb, 1.0f);
}