cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	uint width;
	uint height;
	float rWidth;
	float rHeight;
	matrix view;
	matrix projection;
};

cbuffer ModelConstantBuffer : register(b1)
{
	matrix	model;
	int		material;
};

struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 normal : NORMAL;
	float2 tc : TEXCOORD;
	float3 tangent : TANGENT;
	float3 bitangent : BITANGENT;
};

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 normal : NORMAL;
	float2 tc : TEXCOORD;
	float3 tangent : TANGENT;
	float3 bitangent : BITANGENT;
	int		material : MATERIAL;
};

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);

	pos = mul(pos, model);
	pos = mul(pos, view);
	pos = mul(pos, projection);
	output.pos = pos;

	float4 normal = float4(input.normal, 0.0f);
	normal = mul(normal, model);
	output.normal = normal.xyz;
	output.tc = input.tc;
	output.tangent = input.tangent;
	output.bitangent = input.bitangent;
	output.material = material;

	return output;
}
