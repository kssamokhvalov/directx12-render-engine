#pragma once
#include <stdint.h>
#include "Render/DeviceResources.h"

class Window
{
public:
    Window() :
        mWidthWindow(800),
        mHeightWindow(800),
        mWindowedFullscreen(false){};

    explicit Window(int widthWindow, int heightWindow, bool windowedFullscreen) :
        mWidthWindow(widthWindow),
        mHeightWindow(heightWindow),
        mWindowedFullscreen(windowedFullscreen) {};
    int createWindow(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams);
    HWND GetHwnd() { return mHwnd; }
    uint32_t GetWidth() const { return mWidthWindow; }
    uint32_t GetHeight() const { return mHeightWindow; }
private:
    void SetFullscreen();


	uint32_t mWidthWindow;
	uint32_t mHeightWindow;
	bool mWindowedFullscreen;

    HWND mHwnd = nullptr;
};

