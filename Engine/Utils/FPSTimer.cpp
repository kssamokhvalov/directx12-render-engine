#include "pch.h"
#include "FPSTimer.h"
#include "../pch.h"

bool FPSTimer::frameElapsed()
{
	namespace t = std::chrono;


	Time frame_duration = (Time::now() - mStartTimeFrame);
	if (frame_duration > mMinDurationFrame)
	{
		mRenderingTimeFrame = frame_duration;
		mStartTimeFrame = Time::now();
		return true;
	}
	else
	{
		return false;
	}
}