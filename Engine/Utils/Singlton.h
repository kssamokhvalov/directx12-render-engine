#pragma once

template<class T>
class Singlton
{
public:
	static T& GetInstance() 
	{
		static T s_singlton;
		return s_singlton;
	}

	Singlton(const Singlton&) = delete;
	Singlton(Singlton&&) = delete;
	Singlton& operator = (const Singlton&) = delete;
	Singlton& operator = (Singlton&&) = delete;
protected:
	Singlton() = default;
};

