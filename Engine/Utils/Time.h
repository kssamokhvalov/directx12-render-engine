#pragma once
#include <chrono>
class Time
{
public:
	Time() = default;
	Time(float micro_time) :
		mMicroseconds(micro_time) {}
	Time(std::chrono::duration<float, std::micro> micro_time) :
		mMicroseconds(micro_time) {}
	Time(std::chrono::time_point<std::chrono::high_resolution_clock> micro_time) :
		mMicroseconds(std::chrono::duration_cast<std::chrono::microseconds>(
			micro_time.time_since_epoch())) {}
	float ToSeconds() const;
	float toMilliseconds() const;
	float time() const { return mMicroseconds.count(); }

	Time operator+(const Time& rhs) const;
	Time& operator+=(const Time& rhs);
	Time operator-(const Time& rhs) const;
	Time& operator-=(const Time& rhs);
	bool operator<(const Time& rhs) const;
	bool operator>(const Time& rhs) const;


	static Time now();

private:
	std::chrono::duration<float, std::micro> mMicroseconds;

};
