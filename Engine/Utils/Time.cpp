#include "pch.h"
#include "Time.h"
#include "../pch.h"

float Time::ToSeconds() const
{
    return mMicroseconds.count() / 1000000.0f;
}

float Time::toMilliseconds() const
{
    return mMicroseconds.count() / 1000.0f;
}

Time Time::operator+(const Time& rhs) const
{
    Time ans;

    ans.mMicroseconds = mMicroseconds + rhs.mMicroseconds;

    return ans;
}

Time& Time::operator+=(const Time& rhs)
{
    mMicroseconds += rhs.mMicroseconds;

    return *this;
}

Time Time::operator-(const Time& rhs) const
{
    Time ans;

    ans.mMicroseconds = mMicroseconds - rhs.mMicroseconds;

    return ans;
}

Time& Time::operator-=(const Time& rhs)
{
    mMicroseconds -= rhs.mMicroseconds;

    return *this;
}

bool Time::operator<(const Time& rhs) const
{
    return mMicroseconds < rhs.mMicroseconds;
}

bool Time::operator>(const Time& rhs) const
{
    return mMicroseconds > rhs.mMicroseconds;
}

Time Time::now()
{
    Time ans(std::chrono::high_resolution_clock::now());
    return ans;
}