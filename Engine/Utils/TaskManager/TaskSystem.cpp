#include "pch.h"
#include "TaskSystem.h"

TaskSystem::TaskSystem() :
	mIndex(0)
{
	mThreads.reserve(NUM_THREADS);
	for (uint32_t i = 0; i < NUM_THREADS; i++)
	{
		mThreads.emplace_back([&, i] { WorkLoop(i); });
	}

}

TaskSystem::~TaskSystem() 
{
	for(auto& q : mNotificationQueue) q.Done();
	for(auto& t : mThreads) t.join();
}

void TaskSystem::WorkLoop(uint32_t threadIndex)
{
	while (true)
	{
		std::function<void(uint32_t)> workTask;
		for (uint32_t i = 0; i < NUM_THREADS; i++)
		{
			if (mNotificationQueue[i].TryPop(workTask))
				break;
		}

		if (!workTask && !mNotificationQueue[threadIndex].Pop(workTask))
			return;
		workTask(threadIndex);
	}
}
