#pragma once
#include "NotificationQueue.h"
#include "../Singlton.h"

#include <thread>
#include <vector>

class TaskSystem : public Singlton<TaskSystem>
{
public:
	explicit TaskSystem();
	~TaskSystem();

	template<typename F>
	void Async(F&& task)
	{
		uint32_t index = mIndex++;
		for (uint32_t k = 0; k < NUM_THREADS * mNumTryCycles; k++)
		{
			if(mNotificationQueue[(index + k) % NUM_THREADS].TryPush(std::forward<F>(task)));
		}
		mNotificationQueue[index % NUM_THREADS].Push(std::forward<F>(task));
	}

	const uint32_t							NUM_THREADS{ std::thread::hardware_concurrency() / 2 };
private:
	void WorkLoop(uint32_t threadIndex);
private:
	std::vector<NotificationQueue>			mNotificationQueue{ std::thread::hardware_concurrency() / 2 };
	std::vector<std::thread>				mThreads;
	const uint32_t							mNumTryCycles{ 3 };
	std::atomic<uint32_t>					mIndex;
};

