#pragma once
#include <deque>
#include <mutex>
#include <condition_variable>
#include <functional>

class NotificationQueue
{
public:
	NotificationQueue() = default;
	NotificationQueue(const NotificationQueue&) = delete;
	NotificationQueue(NotificationQueue&&) noexcept = default;
	NotificationQueue& operator=(const NotificationQueue&) = delete;
	NotificationQueue& operator=(NotificationQueue&&) noexcept = default;
	~NotificationQueue() = default;

	bool Pop(std::function<void(uint32_t)>& task);
	bool TryPop(std::function<void(uint32_t)>& task);
	void Done();

	template<typename F>
	void Push(F&& task)
	{
		{
			std::unique_lock<std::mutex> lock(mMutex);
			mQueueTasks.emplace_back(std::forward<F>(task));
		}
		mCv.notify_one();
	}

	template<typename F>
	bool TryPush(F&& task)
	{
		{
			std::unique_lock<std::mutex> lock(mMutex, std::try_to_lock);
			if (!lock) return false;
			mQueueTasks.emplace_back(std::forward<F>(task));
		}
		mCv.notify_one();
		return true;
	}
private:
	std::mutex							mMutex;
	std::deque<std::function<void(uint32_t)>>	mQueueTasks;
	std::condition_variable				mCv;
	std::atomic<bool>					mDone;
};

