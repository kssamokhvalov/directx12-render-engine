#include "pch.h"
#include "NotificationQueue.h"

bool NotificationQueue::Pop(std::function<void(uint32_t)>& task)
{
	std::unique_lock<std::mutex> lock(mMutex);
	mCv.wait(lock, [&] {return !mQueueTasks.empty() || mDone; });
	if (mQueueTasks.empty()) return false;

	task = std::move(mQueueTasks.front());
	mQueueTasks.pop_front();
	return true;
}

bool NotificationQueue::TryPop(std::function<void(uint32_t)>& task)
{
	std::unique_lock<std::mutex> lock(mMutex);
	if (!lock || mQueueTasks.empty()) return false;
	task = std::move(mQueueTasks.front());
	mQueueTasks.pop_front();
	return true;
}

void NotificationQueue::Done()
{
	mDone = true;

	mCv.notify_all();
}
