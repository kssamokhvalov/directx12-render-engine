#pragma once
#include <vector>

namespace engine
{
    using ID = uint32_t;
    using Index = uint32_t;
}

template<typename T>
class SolidVector
{
protected:
    void assertId(engine::ID id) const
    {
        //DEV_ASSERT(id < m_occupied.size());
        //DEV_ASSERT(m_occupied[id]);
    }

public:
    bool occupied(engine::ID id) const { /*DEV_ASSERT(id < m_occupied.size());*/ return m_occupied[id]; }
    engine::ID getNextUnused() const { return m_nextUnused; }

    engine::Index size() const { return engine::Index(m_data.size()); }
    engine::Index sizeMax() const { return engine::Index(m_occupied.size()); }
    engine::Index sizeOccupied() const 
    { 
        engine::Index size = 0;
        for (int i = 0; i < m_occupied.size(); i++)
            if (m_occupied[i] == true)
                size++;
        return size;
    }
    const T* data() const { return m_data.data(); }
    T* data() { return m_data.data(); }

    const T& at(engine::Index index) const { /*DEV_ASSERT(index < m_data.size());*/ return m_data[index]; }
    T& at(engine::Index index) { /*DEV_ASSERT(index < m_data.size());*/ return m_data[index]; }

    const T& operator[](engine::ID id) const { assertId(id); return m_data[m_forwardMap[id]]; }
    T& operator[](engine::ID id) { assertId(id); return m_data[m_forwardMap[id]]; }

    engine::ID insert(const T& value)
    {
        engine::ID id = m_nextUnused;
        //DEV_ASSERT(id <= m_forwardMap.size() && m_forwardMap.size() == m_occupied.size());

        if (id == m_forwardMap.size())
        {
            m_forwardMap.push_back(engine::Index(m_forwardMap.size() + 1));
            m_occupied.push_back(false);
        }

        //DEV_ASSERT(!m_occupied[id]);

        m_nextUnused = m_forwardMap[id];
        m_forwardMap[id] = engine::Index(m_data.size());
        m_occupied[id] = true;

        m_data.emplace_back(value);
        m_backwardMap.emplace_back(id);

        return id;
    }

    void erase(engine::ID id)
    {
        //DEV_ASSERT(id < m_forwardMap.size() && m_forwardMap.size() == m_occupied.size());

        engine::Index& forwardIndex = m_forwardMap[id];
        //DEV_ASSERT(m_occupied[id]);

        m_data[forwardIndex] = std::move(m_data.back());
        m_data.pop_back();

        engine::ID backwardIndex = m_backwardMap.back();

        m_backwardMap[forwardIndex] = backwardIndex;
        m_backwardMap.pop_back();

        m_forwardMap[backwardIndex] = forwardIndex;

        forwardIndex = m_nextUnused;
        m_occupied[id] = false;
        m_nextUnused = id;
    }

    void clear()
    {
        m_forwardMap.clear();
        m_backwardMap.clear();
        m_occupied.clear();
        m_data.clear();
        m_nextUnused = 0;
    }

    void reserve(engine::Index count)
    {
        m_data.reserve(count);
        m_forwardMap.reserve(count);
        m_backwardMap.reserve(count);
        m_occupied.reserve(count);
    }

protected:
    std::vector<T> m_data;
    std::vector<engine::Index> m_forwardMap;
    std::vector<engine::ID> m_backwardMap;
    std::vector<bool> m_occupied;

    engine::ID m_nextUnused = 0;
};