#pragma once
#include <chrono>
#include "time.h"
class FPSTimer
{
public:
	FPSTimer()
		: mMinDurationFrame(1000000 / 60), mRenderingTimeFrame(0), mStartTimeFrame(std::chrono::high_resolution_clock::now())
	{}

	bool frameElapsed();
	Time getRenderingTimeFrame() const { return mRenderingTimeFrame; }
private:
	Time mMinDurationFrame; //microsecond
	Time mRenderingTimeFrame; //microsecond
	Time mStartTimeFrame;
};
