#pragma once

#include <Utils/FPSTimer.h>
#include <Utils/Singlton.h>
#include <Render/Camera.h>
#include <Render/Scene.h>

#include <Window.h>
#include <vector>

enum ButtonsEvents {
	LBUTTON_MOUSE = VK_LBUTTON,
	RBUTTON_MOUSE = VK_RBUTTON,
	SPACE = VK_SPACE,
	CTRL = VK_CONTROL,
	SHIFT = VK_SHIFT,
	A = 0x41,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z = 0x5A,
	ADD = VK_ADD,
	SUBTRACT = VK_SUBTRACT
};



class Application : public Singlton<Application>
{
public:

	struct MousePos
	{
		int x;
		int y;
	};

	struct DifferentPos
	{
		float x;
		float y;
	};

	void Init(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams);
	void Tick();
	void ButtonPressed(ButtonsEvents button, int x, int y);
	void ButtonReleased(ButtonsEvents button);
	void OnMouseMove(int x, int y);
private:
	void UpdateDiffMouse(int x, int y);
	void ProcessInput();
	void LoadLevel();
private:
	const float BASE_SPEED_CAMERA = 1.0f;
	const float SPEED_ROTATE_CAMERA = 70.0f * 3.14f / 180.0f;
	const float SENSITIVITY_CAMERA = 20.f;

	Window mWindow;

	std::vector<ButtonsEvents> mPressedButtons;

	MousePos mMousePos;
	DifferentPos mMouseDifferent;

	FPSTimer mGameTimer;

	Camera mCamera;

	Scene mScene;
};

