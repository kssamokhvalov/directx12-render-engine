#include "pch.h"
#include "Application.h"
#include <stdio.h>
#include <thread>
void initConsole()
{
    AllocConsole();
    FILE* dummy;
    auto s = freopen_s(&dummy, "CONOUT$", "w", stdout); // stdout will print to the newly created console
}

LPCWSTR g_szClassName = L"myWindowClass";

// Step 4: the Window Procedure
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_RBUTTONDOWN:
        Application::GetInstance().ButtonPressed(ButtonsEvents::RBUTTON_MOUSE, LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_RBUTTONUP:
        Application::GetInstance().ButtonReleased(ButtonsEvents::RBUTTON_MOUSE);
        break;

    case WM_LBUTTONDOWN:
        Application::GetInstance().ButtonPressed(ButtonsEvents::LBUTTON_MOUSE, LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_LBUTTONUP:
        Application::GetInstance().ButtonReleased(ButtonsEvents::LBUTTON_MOUSE);
        break;

    case WM_MOUSEWHEEL: {
        //Application::GetInstance().OnMouseWheelScroll(GET_WHEEL_DELTA_WPARAM(wParam));
        break;
    }
    case WM_MOUSEMOVE:
        Application::GetInstance().OnMouseMove(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_KEYDOWN:
        Application::GetInstance().ButtonPressed(static_cast<ButtonsEvents>(wParam), LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYUP:
        Application::GetInstance().ButtonReleased(static_cast<ButtonsEvents>(wParam));
        break;
    case WM_SIZE:
        //Application::GetInstance().Resize(LOWORD(lParam), HIWORD(lParam));
        break;
    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
    initConsole();
#endif // DEBUG

    Application::GetInstance().Init(hInstance, WndProc, nCmdShow);

    MSG Msg;
    while (true)
    {
        while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE) > 0)
        {
            if (Msg.message == WM_QUIT) {
                return static_cast<int>(Msg.wParam);
            }
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
        Application::GetInstance().Tick();
        //std::this_thread::yield();
    }
}