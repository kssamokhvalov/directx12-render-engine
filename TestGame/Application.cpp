#include "pch.h"
#include "Application.h"
#include "Engine.h"
#include <iostream>
#include <algorithm>

void Application::Init(HINSTANCE appHandle, WNDPROC WinProc, int windowShowParams)
{
	Engine::Init();


	mCamera.SetWorldPosition(DirectX::XMVectorSet(0.f, 0.f, -2.f, 0.f));
	mCamera.SetWorldAngles(DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f));
	mPressedButtons.reserve(4);

	mWindow.createWindow(appHandle, WinProc, windowShowParams);
	DX::DeviceResources::GetInstance().SetHWND(mWindow.GetHwnd());

	Renderer::GetInstance().Init();

	LoadLevel();
}

void Application::Tick()
{
	if (mGameTimer.frameElapsed())
	{
		mGameTimer.getRenderingTimeFrame().time();
		ProcessInput();
		Renderer::GetInstance().Update();
		mCamera.UpdateMatrix();
		Renderer::GetInstance().UpdateViewMatrix(mCamera);

		Renderer::GetInstance().DefferedRenderScene(&mScene);
		Renderer::GetInstance().LightRenderScene(&mScene);

		DX::DeviceResources::GetInstance().Present();
		//TEST
		std::function<void(uint32_t)> tasks = [](uint32_t idThread) { std::cout << "Hello! I'm thread:" << idThread << std::endl; };
		//TaskSystem::GetInstance().Async(tasks);
		//TEST
	}
}

void Application::ButtonPressed(ButtonsEvents button, int x, int y)
{
	switch (button)
	{
	case LBUTTON_MOUSE:
	{
		mMousePos.x = x;
		mMousePos.y = y;
		UpdateDiffMouse(x, y);
		break;
	}
	default:
		break;
	}

	auto idButton = std::find(mPressedButtons.begin(), mPressedButtons.end(), button);

	if(idButton == mPressedButtons.end())
		mPressedButtons.push_back(button);
}

void Application::ButtonReleased(ButtonsEvents button)
{
	auto idButton = std::find(mPressedButtons.begin(), mPressedButtons.end(), button);
	if(idButton != mPressedButtons.end())
		mPressedButtons.erase(idButton);
}

void Application::OnMouseMove(int x, int y)
{
	UpdateDiffMouse(x, y);
}

void Application::UpdateDiffMouse(int x, int y)
{
	mMouseDifferent.x = static_cast<float>(x - mMousePos.x) / (static_cast<float>(mWindow.GetWidth()));
	mMouseDifferent.y = static_cast<float>(y - mMousePos.y) / (static_cast<float>(mWindow.GetHeight()));

	mMousePos.x = x;
	mMousePos.y = y;
}

void Application::ProcessInput()
{
	float durationFrame = mGameTimer.getRenderingTimeFrame().ToSeconds();
	float speedCamera = BASE_SPEED_CAMERA * durationFrame;

	for (const auto button : mPressedButtons)
	{
		switch (button)
		{
		case LBUTTON_MOUSE:
		{
			float diffX = mMouseDifferent.x * SPEED_ROTATE_CAMERA * SENSITIVITY_CAMERA;
			float diffY = mMouseDifferent.y * SPEED_ROTATE_CAMERA * SENSITIVITY_CAMERA;

			mCamera.AddRelativeAngles({ 0.0f, diffY, diffX });

			mMouseDifferent.x = 0.f;
			mMouseDifferent.y = 0.f;
			break;
		}
		case ButtonsEvents::W:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(0.f, 0.f, speedCamera, 0.f));
			break;
		case ButtonsEvents::S:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(0.f, 0.f, -speedCamera, 0.f));
			break;
		case ButtonsEvents::A:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(-speedCamera, 0.f, 0.f, 0.f));
			break;
		case ButtonsEvents::D:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(speedCamera, 0.f, 0.f, 0.f));
			break;
		case ButtonsEvents::Q:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(0.f, -speedCamera, 0.f, 0.f));
			break;
		case ButtonsEvents::E:
			mCamera.AddRelativePosition(DirectX::XMVectorSet(0.f, speedCamera, 0.f, 0.f));
			break;
		default:
			break;
		}
	}
}

void Application::LoadLevel()
{
	Model* model = ModelManager::GetInstance().GetModel("assets\\models\\EastTower\\EastTower.fbx");
	


	std::vector<OpaqueTextures> tower = {
	{
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/CityWalls_BaseColor.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/CityWalls_Normal.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/CityWalls_Roughness.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/CityWalls_Metallic.dds"),
		0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
	},
	{
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Marble_BaseColor.dds"),
		TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
		TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
		TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
		0, HasTextures::Albedo
	},
	{
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Trims_BaseColor.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Trims_Normal.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Trims_Roughness.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Trims_Metallic.dds"),
		0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
	},
	{
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Statue_BaseColor.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Statue_Normal.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Statue_Roughness.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/Statue_Metallic.dds"),
		0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
	},
	{
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/StoneWork_BaseColor.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/StoneWork_Normal.dds"),
		TextureManager::GetInstance().GetTexture("assets/models/EastTower/dds/StoneWork_Roughness.dds"),
		TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
		0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness
	}
	};

	std::vector<OpaqueTextures> samurai = {
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Sword_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Sword_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Sword_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Sword_Metallic.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Head_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Head_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Head_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Eyes_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Eyes_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
			TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Helmet_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Helmet_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Helmet_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Helmet_Metallic.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Decor_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Decor_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Decor_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Decor_Metallic.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Pants_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Pants_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Pants_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Pants_Metallic.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Hands_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Hands_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Hands_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/textures/missingTexture.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness
		},
		{
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Torso_BaseColor.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Torso_Normal.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Torso_Roughness.dds"),
			TextureManager::GetInstance().GetTexture("assets/models/Samurai/dds/Torso_Metallic.dds"),
			0, HasTextures::Albedo | HasTextures::Metalness | HasTextures::Roughness | HasTextures::Metalness
		}
	};

	DirectX::XMFLOAT4X4 matrix;
	DirectX::XMStoreFloat4x4(&matrix, DirectX::XMMatrixAffineTransformation(
		DirectX::XMVectorSet(1.f, 1.f, 1.f, 0.f),
		DirectX::XMVectorSet(1.f, 0.f, 0.f, 0.f),
		DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f),
		DirectX::XMVectorSet(0.f, 0.f, 5.f, 0.f)));
	mScene.AddModel({ model, tower }, matrix);



	model = ModelManager::GetInstance().GetModel("assets\\models\\Samurai\\Samurai.fbx");
	DirectX::XMStoreFloat4x4(&matrix, DirectX::XMMatrixAffineTransformation(
		DirectX::XMVectorSet(1.f, 1.f, 1.f, 0.f),
		DirectX::XMVectorSet(1.f, 0.f, 0.f, 0.f),
		DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f),
		DirectX::XMVectorSet(5.f, 0.f, 5.f, 0.f)));
	mScene.AddModel({ model, samurai }, matrix);


	DirectX::XMStoreFloat4x4(&matrix, DirectX::XMMatrixAffineTransformation(
		DirectX::XMVectorSet(1.f, 1.f, 1.f, 0.f),
		DirectX::XMVectorSet(1.f, 0.f, 0.f, 0.f),
		DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f),
		DirectX::XMVectorSet(0.f, 0.f, 4.f, 0.f)));
	mScene.AddSphereLight(matrix, { { 0.647058845f, 0.164705887f, 0.164705887f }, 1000.0f, 0.25f, 5.0f });

	DirectX::XMStoreFloat4x4(&matrix, DirectX::XMMatrixAffineTransformation(
		DirectX::XMVectorSet(1.f, 1.f, 1.f, 0.f),
		DirectX::XMVectorSet(1.f, 0.f, 0.f, 0.f),
		DirectX::XMVectorSet(0.f, 0.f, 0.f, 1.f),
		DirectX::XMVectorSet(3.f, 0.f, 3.f, 0.f)));
	mScene.AddSphereLight(matrix, { { 0.647058845f, .0f, .0f }, 1000.0f, 0.25f, 4.0f });
}
